package au.carrsq.sensorplatform.Synchronization;

/**
 * Created by julia on 18.04.2017.
 */

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SyncResult;
import android.database.Cursor;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.net.HttpURLConnection;
import java.util.ArrayList;

import au.carrsq.sensorplatform.Database.SqliteDataProvider;
import au.carrsq.sensorplatform.Utilities.IO;


public class SyncAdapter extends AbstractThreadedSyncAdapter {

    public static final String TAG = "SyncAdapter";
    private String accessToken = null;
    private Authenticator authenticator;
    private SyncUtils syncUtils;

    ContentResolver mContentResolver;

    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        mContentResolver = context.getContentResolver();
        authenticator = new Authenticator(context);
        syncUtils = new SyncUtils(context);
        Log.d(TAG, "created Adapter");
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
        Log.i(TAG, "Perform sync");
        WifiManager wifiManager = (WifiManager) getContext().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (wifiManager.isWifiEnabled()) {
            synchronizeTables();
        }
        // synchronizeTables();
        getContext().getApplicationContext().getContentResolver().notifyChange(SqliteDataProvider.RAW_DATA_URI, null, false);
    }


    public void synchronizeTables() {
        if (accessToken == null) {
            accessToken = authenticator.getAuthTokenLabel("");
            Log.d(TAG, "accessToken: " + accessToken);
            if (accessToken != null) {
                synchronizeTables();
            }
        } else {

            syncRawDataTable();
            WifiManager wifiManager = (WifiManager) getContext().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            if (wifiManager.isWifiEnabled()) {
                syncEventDataTable();
                syncPhoneInteratctionDataTable();
                syncMvnEventDataTable();
                syncMvnLikelihoodDataTable();
            }
            if (wifiManager.isWifiEnabled()) {
            }
            if (wifiManager.isWifiEnabled()) {
            }
            if (wifiManager.isWifiEnabled()) {
            }
        }
    }

    public void syncRawDataTable() {
        Log.d(TAG, "Start sync raw data table");
        try {
            if (Looper.myLooper() == null) {
                Looper.prepare();
            }
            Cursor cursor = mContentResolver.query(SqliteDataProvider.RAW_DATA_URI, null, "sync = ?", new String[]{"0"}, null);
            if (cursor.getCount() != 0) {
                JSONObject jObj;
                JSONArray jArr = new JSONArray();
                cursor.moveToFirst();

                // counts number of elements
                int elementCounter = 0;
                // List of JSONArray containing 50 JSONObjects each
                ArrayList<JSONArray> mylist = new ArrayList<JSONArray>();

                do {
                    jObj = new JSONObject();
                    jObj.put("code_timestamp_id", cursor.getString(cursor.getColumnIndex("CODE_ID")) + "_" + cursor.getString(cursor.getColumnIndex("DATETIME")));
                    jObj.put("code_id", cursor.getString(cursor.getColumnIndex("CODE_ID")));
                    jObj.put("timestamp", cursor.getInt(cursor.getColumnIndex("TIMESTAMP")));
                    jObj.put("datetime", cursor.getString(cursor.getColumnIndex("DATETIME")));

                    jObj.put("accelerationX", cursor.getDouble(cursor.getColumnIndex("ACCELERATIONX")));
                    jObj.put("accelerationY", cursor.getDouble(cursor.getColumnIndex("ACCELERATIONY")));
                    jObj.put("accelerationZ", cursor.getDouble(cursor.getColumnIndex("ACCELERATIONZ")));
                    jObj.put("rawAccelerationX", cursor.getDouble(cursor.getColumnIndex("RAWACCELERATIONX")));
                    jObj.put("rawAccelerationY", cursor.getDouble(cursor.getColumnIndex("RAWACCELERATIONY")));
                    jObj.put("rawAccelerationZ", cursor.getDouble(cursor.getColumnIndex("RAWACCELERATIONZ")));

                    jObj.put("gravityX", cursor.getDouble(cursor.getColumnIndex("GRAVITYX")));
                    jObj.put("gravityY", cursor.getDouble(cursor.getColumnIndex("GRAVITYY")));
                    jObj.put("gravityZ", cursor.getDouble(cursor.getColumnIndex("GRAVITYZ")));

                    jObj.put("gyroscopeX", cursor.getFloat(cursor.getColumnIndex("GYROSCOPEX")));
                    jObj.put("gyroscopeY", cursor.getFloat(cursor.getColumnIndex("GYROSCOPEY")));
                    jObj.put("gyroscopeZ", cursor.getFloat(cursor.getColumnIndex("GYROSCOPEZ")));

                    jObj.put("rotationX", cursor.getDouble(cursor.getColumnIndex("ROTATIONX")));
                    jObj.put("rotationY", cursor.getDouble(cursor.getColumnIndex("ROTATIONY")));
                    jObj.put("rotationZ", cursor.getDouble(cursor.getColumnIndex("ROTATIONZ")));
                    jObj.put("rotationXRad", cursor.getDouble(cursor.getColumnIndex("ROTATIONXRAD")));
                    jObj.put("rotationYRad", cursor.getDouble(cursor.getColumnIndex("ROTATIONYRAD")));
                    jObj.put("rotationZRad", cursor.getDouble(cursor.getColumnIndex("ROTATIONZRAD")));

                    jObj.put("latitude", cursor.getDouble(cursor.getColumnIndex("LATITUDE")));
                    jObj.put("longitude", cursor.getDouble(cursor.getColumnIndex("LONGITUDE")));

                    jObj.put("speedCalculated", cursor.getDouble(cursor.getColumnIndex("SPEEDCALCULATED")));
                    jObj.put("speedLocationManager", cursor.getDouble(cursor.getColumnIndex("SPEEDLOCATIONMANAGER")));

                    jObj.put("jerk", cursor.getDouble(cursor.getColumnIndex("JERK")));
                    jObj.put("steer", cursor.getDouble(cursor.getColumnIndex("STEER")));
                    jObj.put("gbea", cursor.getDouble(cursor.getColumnIndex("GBEA")));
                    jObj.put("gacc", cursor.getDouble(cursor.getColumnIndex("GACC")));
                    jObj.put("magneticFieldX", cursor.getDouble(cursor.getColumnIndex("MAGNETICFIELDX")));
                    jObj.put("magneticFieldY", cursor.getDouble(cursor.getColumnIndex("MAGNETICFIELDY")));
                    jObj.put("magneticFieldZ", cursor.getDouble(cursor.getColumnIndex("MAGNETICFIELDZ")));

                    jArr.put(jObj);
                    elementCounter += 1;

                    // make ArrayList containing 50 elements in JSONArray each array entry
                    if (elementCounter == SyncUtils.STEP_SIZE || cursor.isLast()) {
                        mylist.add(jArr);
                        jArr = new JSONArray();
                        elementCounter = 0;
                    }
                }
                while (cursor.moveToNext());

                String urlString = SyncUtils.BASE_URL + "/RawData?access_token=" + accessToken;
                int positionOfCursor = 0;
                for (JSONArray jarr : mylist) {
                    String dataJArray = jarr.toString();
                    HttpURLConnection conn = syncUtils.postDataToServer(dataJArray, urlString, "POST");
                    handleResponseForDataTables(cursor, conn, SqliteDataProvider.RAW_DATA_URI, positionOfCursor);

                    positionOfCursor += SyncUtils.STEP_SIZE;
                }
                cursor.close();
            }

        } catch (Exception e) {
            e.getMessage().toString();
            File output = new File(IO.baseDir + File.separator + System.currentTimeMillis() / 1000 + "_error.txt");
            IO.writeFile(output, e.getMessage() + ": " + e.getStackTrace());
        }
    }

    public void syncEventDataTable() {
        Log.d(TAG, "Start sync event data table");
        try {
            Cursor cursor = mContentResolver.query(SqliteDataProvider.EVENT_DATA_URI, null, "sync = ?", new String[]{"0"}, null);
            if (cursor.getCount() != 0) {
                JSONObject jObj;
                JSONArray jArr = new JSONArray();
                cursor.moveToFirst();

                // counts number of elements
                int elementCounter = 0;
                // List of JSONArray containing 50 JSONObjects each
                ArrayList<JSONArray> mylist = new ArrayList<JSONArray>();
                do {
                    jObj = new JSONObject();
                    jObj.put("code_timestamp_id", cursor.getString(cursor.getColumnIndex("CODE_ID")) + "_" + cursor.getString(cursor.getColumnIndex("DATETIME")));
                    jObj.put("code_id", cursor.getString(cursor.getColumnIndex("CODE_ID")));
                    jObj.put("timestamp", cursor.getInt(cursor.getColumnIndex("TIMESTAMP")));
                    jObj.put("datetime", cursor.getString(cursor.getColumnIndex("DATETIME")));

                    jObj.put("level", cursor.getInt(cursor.getColumnIndex("LEVEL")));
                    jObj.put("description", cursor.getString(cursor.getColumnIndex("DESCRIPTION")));
                    jObj.put("value", cursor.getDouble(cursor.getColumnIndex("VALUE")));
                    jObj.put("extra", cursor.getDouble(cursor.getColumnIndex("EXTRA")));
                    jObj.put("videofront", cursor.getString(cursor.getColumnIndex("VIDEOFRONT")));
                    jObj.put("videoback", cursor.getString(cursor.getColumnIndex("VIDEOBACK")));

                    jArr.put(jObj);
                    elementCounter += 1;

                    // make ArrayList containing 50 elements in JSONArray each array entry
                    if (elementCounter == SyncUtils.STEP_SIZE || cursor.isLast()) {
                        mylist.add(jArr);
                        jArr = new JSONArray();
                        elementCounter = 0;
                    }
                }
                while (cursor.moveToNext());


                String urlString = SyncUtils.BASE_URL + "/Events?access_token=" + accessToken;
                int positionOfCursor = 0;
                for (JSONArray jarr : mylist) {
                    String dataJArray = jarr.toString();
                    HttpURLConnection conn = syncUtils.postDataToServer(dataJArray, urlString, "POST");
                    handleResponseForDataTables(cursor, conn, SqliteDataProvider.EVENT_DATA_URI, positionOfCursor);

                    positionOfCursor += SyncUtils.STEP_SIZE;
                }
                cursor.close();
            }

        } catch (Exception e) {
            e.getMessage().toString();
            File output = new File(IO.baseDir + File.separator + System.currentTimeMillis() / 1000 + "_error.txt");
            IO.writeFile(output, e.getMessage() + ": " + e.getStackTrace());
        }
    }

    public void syncPhoneInteratctionDataTable() {
        Log.d(TAG, "Start sync interaction data table");
        try {
            Cursor cursor = mContentResolver.query(SqliteDataProvider.PHONE_INTERACTION_DATA_URI, null, "sync = ?", new String[]{"0"}, null);
            if (cursor.getCount() != 0) {
                JSONObject jObj;
                JSONArray jArr = new JSONArray();
                cursor.moveToFirst();

                // counts number of elements
                int elementCounter = 0;
                // List of JSONArray containing 50 JSONObjects each
                ArrayList<JSONArray> mylist = new ArrayList<JSONArray>();
                do {
                    jObj = new JSONObject();
                    jObj.put("code_timestamp_id", cursor.getString(cursor.getColumnIndex("CODE_ID")) + "_" + cursor.getString(cursor.getColumnIndex("DATETIME")));
                    jObj.put("code_id", cursor.getString(cursor.getColumnIndex("CODE_ID")));
                    jObj.put("timestamp", cursor.getInt(cursor.getColumnIndex("TIMESTAMP")));
                    jObj.put("datetime", cursor.getString(cursor.getColumnIndex("DATETIME")));

                    jObj.put("interaction", cursor.getString(cursor.getColumnIndex("INTERACTION")));

                    jArr.put(jObj);
                    elementCounter += 1;

                    // make ArrayList containing 50 elements in JSONArray each array entry
                    if (elementCounter == SyncUtils.STEP_SIZE || cursor.isLast()) {
                        mylist.add(jArr);
                        jArr = new JSONArray();
                        elementCounter = 0;
                    }
                }
                while (cursor.moveToNext());

                String urlString = SyncUtils.BASE_URL + "/PhoneInteractions?access_token=" + accessToken;
                int positionOfCursor = 0;
                for (JSONArray jarr : mylist) {
                    String dataJArray = jarr.toString();
                    HttpURLConnection conn = syncUtils.postDataToServer(dataJArray, urlString, "POST");
                    handleResponseForDataTables(cursor, conn, SqliteDataProvider.PHONE_INTERACTION_DATA_URI, positionOfCursor);

                    positionOfCursor += SyncUtils.STEP_SIZE;
                }
                cursor.close();
            }

        } catch (Exception e) {
            e.getMessage().toString();
            File output = new File(IO.baseDir + File.separator + System.currentTimeMillis() / 1000 + "_error.txt");
            IO.writeFile(output, e.getMessage() + ": " + e.getStackTrace());
        }
    }

    public void syncMvnEventDataTable() {
        Log.d(TAG, "Start sync mvn event data table");
        try {
            Cursor cursor = mContentResolver.query(SqliteDataProvider.MVN_EVENT_DATA_URI, null, "sync = ?", new String[]{"0"}, null);
            if (cursor.getCount() != 0) {
                JSONObject jObj;
                JSONArray jArr = new JSONArray();
                cursor.moveToFirst();

                // counts number of elements
                int elementCounter = 0;
                // List of JSONArray containing 50 JSONObjects each
                ArrayList<JSONArray> mylist = new ArrayList<JSONArray>();
                do {
                    jObj = new JSONObject();
                    jObj.put("code_timestamp_id", cursor.getString(cursor.getColumnIndex("CODE_ID")) + "_" + cursor.getString(cursor.getColumnIndex("DATETIME")));
                    jObj.put("code_id", cursor.getString(cursor.getColumnIndex("CODE_ID")));
                    jObj.put("timestamp", cursor.getInt(cursor.getColumnIndex("TIMESTAMP")));
                    jObj.put("datetime", cursor.getString(cursor.getColumnIndex("DATETIME")));

                    jObj.put("event", cursor.getString(cursor.getColumnIndex("EVENT")));
                    jObj.put("severity", cursor.getString(cursor.getColumnIndex("SEVERITY")));

                    jArr.put(jObj);
                    elementCounter += 1;

                    // make ArrayList containing 50 elements in JSONArray each array entry
                    if (elementCounter == SyncUtils.STEP_SIZE || cursor.isLast()) {
                        mylist.add(jArr);
                        jArr = new JSONArray();
                        elementCounter = 0;
                    }
                }
                while (cursor.moveToNext());


                String urlString = SyncUtils.BASE_URL + "/MvnEventDatas?access_token=" + accessToken;
                int positionOfCursor = 0;
                for (JSONArray jarr : mylist) {
                    String dataJArray = jarr.toString();
                    HttpURLConnection conn = syncUtils.postDataToServer(dataJArray, urlString, "POST");
                    handleResponseForDataTables(cursor, conn, SqliteDataProvider.MVN_EVENT_DATA_URI, positionOfCursor);

                    positionOfCursor += SyncUtils.STEP_SIZE;
                }
                cursor.close();
            }

        } catch (Exception e) {
            e.getMessage().toString();
            File output = new File(IO.baseDir + File.separator + System.currentTimeMillis() / 1000 + "_error.txt");
            IO.writeFile(output, e.getMessage() + ": " + e.getStackTrace());
        }
    }

    public void syncMvnLikelihoodDataTable() {
        Log.d(TAG, "Start sync mvn likelihood data table");
        try {
            Cursor cursor = mContentResolver.query(SqliteDataProvider.MVN_LIKELIHOOD_DATA_URI, null, "sync = ?", new String[]{"0"}, null);
            if (cursor.getCount() != 0) {
                JSONObject jObj;
                JSONArray jArr = new JSONArray();
                cursor.moveToFirst();

                // counts number of elements
                int elementCounter = 0;
                // List of JSONArray containing 50 JSONObjects each
                ArrayList<JSONArray> mylist = new ArrayList<JSONArray>();
                do {
                    jObj = new JSONObject();
                    jObj.put("code_timestamp_id", cursor.getString(cursor.getColumnIndex("CODE_ID")) + "_" + cursor.getString(cursor.getColumnIndex("DATETIME")));
                    jObj.put("code_id", cursor.getString(cursor.getColumnIndex("CODE_ID")));
                    jObj.put("timestamp", cursor.getInt(cursor.getColumnIndex("TIMESTAMP")));
                    jObj.put("datetime", cursor.getString(cursor.getColumnIndex("DATETIME")));

                    jObj.put("logindex", cursor.getString(cursor.getColumnIndex("LOGINDEX")));
                    jObj.put("alternative", cursor.getString(cursor.getColumnIndex("ALTERNATIVE")));
                    jObj.put("likelihood", cursor.getString(cursor.getColumnIndex("LIKELIHOOD")));

                    jArr.put(jObj);
                    elementCounter += 1;

                    // make ArrayList containing 50 elements in JSONArray each array entry
                    if (elementCounter == SyncUtils.STEP_SIZE || cursor.isLast()) {
                        mylist.add(jArr);
                        jArr = new JSONArray();
                        elementCounter = 0;
                    }
                }
                while (cursor.moveToNext());


                String urlString = SyncUtils.BASE_URL + "/MvnLikelihoodDatas?access_token=" + accessToken;
                int positionOfCursor = 0;
                for (JSONArray jarr : mylist) {
                    String dataJArray = jarr.toString();
                    HttpURLConnection conn = syncUtils.postDataToServer(dataJArray, urlString, "POST");
                    handleResponseForDataTables(cursor, conn, SqliteDataProvider.MVN_LIKELIHOOD_DATA_URI, positionOfCursor);

                    positionOfCursor += SyncUtils.STEP_SIZE;
                }
                cursor.close();
            }

        } catch (Exception e) {
            e.getMessage().toString();
            File output = new File(IO.baseDir + File.separator + System.currentTimeMillis() / 1000 + "_error.txt");
            IO.writeFile(output, e.getMessage() + ": " + e.getStackTrace());
        }
    }

    public void handleResponseForDataTables(Cursor cursor, HttpURLConnection conn, Uri inputUri, int position) {
        Log.d(TAG, "Handle response from server...");
        try {
            if (conn != null && 200 <= conn.getResponseCode() && conn.getResponseCode() <= 299) {
                //cursor.moveToFirst();
                cursor.moveToPosition(position);

                ContentValues syncDone = new ContentValues();
                syncDone.put("sync", 1);
                // int NumberOfRowsToUpdate = cursor.getCount();
                int count = 0;
                do {
                    String codeIdToUpdate = cursor.getString(cursor.getColumnIndex("CODE_ID"));
                    String dateTimeToUpdate = cursor.getString(cursor.getColumnIndex("DATETIME"));
                    int syncFinished = mContentResolver.update(inputUri, syncDone, "CODE_ID = ? AND DATETIME = ?", new String[]{codeIdToUpdate, dateTimeToUpdate});
                    count++;
                } while (cursor.moveToNext() && count != SyncUtils.STEP_SIZE);
                conn.disconnect();

            } else {
                if (conn != null) {
                    handleErrorCodesServerForTables(conn);
                    JSONObject serverResponseJsonObject = syncUtils.getServerResponseJsonObject(conn);
                    Log.d("POST-Exception", serverResponseJsonObject.getString("error"));

                    File output = new File(IO.baseDir + File.separator + System.currentTimeMillis() / 1000 + "_error.txt");
                    IO.writeFile(output, serverResponseJsonObject.toString());
                    conn.disconnect();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void handleErrorCodesServerForTables(HttpURLConnection conn) {
        JSONObject serverResponseJsonObject = syncUtils.getServerResponseJsonObject(conn);
        try {
            JSONObject error = serverResponseJsonObject.getJSONObject("error");
            switch (conn.getResponseCode()) {
                case 401:
                    if (error.getString("code").equals("INVALID_TOKEN")) {
                        accessToken = authenticator.getAuthTokenLabel("");
                    }
                    break;
                case 500:
                    if (error.getString("name").equals("ArrayOfErrors")) {
                        JSONArray details = error.getJSONArray("details");
                        for (int i = 0; i < details.length(); i++) {
                            Log.d(TAG, details.getJSONObject(i).get("message").toString());
                            // @TODO: remove this!
                            File output = new File(IO.baseDir + File.separator + System.currentTimeMillis() / 1000 + "_duplicate_entry.txt");
                            IO.writeFile(output, details.getJSONObject(i).get("message").toString());
                        }

                    }
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}


