package au.carrsq.sensorplatform.Synchronization;

/**
 * Created by julia on 07.06.2017.
 */

public class ServerAccount {

    private String email;
    private String password;

    public ServerAccount(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
