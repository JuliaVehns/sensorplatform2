package au.carrsq.sensorplatform.Synchronization;

import android.content.Context;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by julia on 07.06.2017.
 */

public class SyncUtils {
    public static final String TAG = "SyncUtils";
    public static final String BASE_URL = "https://sensorplatformbackend.herokuapp.com/api";
    public static final int STEP_SIZE = 250;

    public SyncUtils(Context context) {
//        ContentResolver mContentResolver = context.getContentResolver();
    }

    public HttpURLConnection postDataToServer(String dataJArray, String urlString, String method) {
        Log.d(TAG, "Post data to server: " + urlString);
        URL url = null;
        try {
            url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(5000);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestMethod(method);

            OutputStream os = conn.getOutputStream();
            os.write(dataJArray.getBytes("UTF-8"));
            os.close();
            return conn;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public JSONObject getServerResponseJsonObject(HttpURLConnection conn) {
        BufferedReader br;
        try {
            try {
                br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            } catch (IOException exception) {
                br = new BufferedReader(new InputStreamReader((conn.getErrorStream())));
            }
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            JSONObject json = new JSONObject(sb.toString());
            return json;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }


}
