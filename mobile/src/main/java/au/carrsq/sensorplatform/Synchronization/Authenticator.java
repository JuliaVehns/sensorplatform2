package au.carrsq.sensorplatform.Synchronization;

import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.NetworkErrorException;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import org.json.JSONObject;

import java.net.HttpURLConnection;

/**
 * Created by julia on 18.04.2017.
 */

/*
 * Implement AbstractAccountAuthenticator and stub out all
 * of its methods
 */
public class Authenticator extends AbstractAccountAuthenticator {
    // Simple constructor
    private ServerAccount account;
    private static final String TAG = "Authenticator";
    private SyncUtils syncUtils;


    public Authenticator(Context context) {
        super(context);
        account = new ServerAccount("j.vehns@qut.edu.au", "pass");
        syncUtils = new SyncUtils(context);
    }

    // Editing properties is not supported
    @Override
    public Bundle editProperties(
            AccountAuthenticatorResponse r, String s) {
        throw new UnsupportedOperationException();
    }

    // Don't add additional accounts
    @Override
    public Bundle addAccount(
            AccountAuthenticatorResponse r,
            String s,
            String s2,
            String[] strings,
            Bundle bundle) throws NetworkErrorException {
        return null;
    }

    // Ignore attempts to confirm credentials
    @Override
    public Bundle confirmCredentials(
            AccountAuthenticatorResponse r,
            Account account,
            Bundle bundle) throws NetworkErrorException {
        return null;
    }

    // Getting an authentication token is not supported
    @Override
    public Bundle getAuthToken(
            AccountAuthenticatorResponse r,
            Account account,
            String s,
            Bundle bundle) throws NetworkErrorException {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getAuthTokenLabel(String s) {
        JSONObject userLoginJson = new JSONObject();
        try {
            userLoginJson.put("email", account.getEmail());
            userLoginJson.put("password", account.getPassword());

            String urlString = syncUtils.BASE_URL + "/Users/login";

            HttpURLConnection conn = syncUtils.postDataToServer(userLoginJson.toString(), urlString, "POST");

            if (conn != null && 200 <= conn.getResponseCode() && conn.getResponseCode() <= 299) {
                JSONObject serverResponse = syncUtils.getServerResponseJsonObject(conn);
                Log.d(TAG, "Got AuthToken");
                return  serverResponse.getString("id");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d(TAG, "Failed to get AuthToken");
        return null;
    }

    // Updating user credentials is not supported
    @Override
    public Bundle updateCredentials(
            AccountAuthenticatorResponse r,
            Account account,
            String s, Bundle bundle) throws NetworkErrorException {
        throw new UnsupportedOperationException();
    }

    // Checking features for the account is not supported
    @Override
    public Bundle hasFeatures(
            AccountAuthenticatorResponse r,
            Account account, String[] strings) throws NetworkErrorException {
        throw new UnsupportedOperationException();
    }
}