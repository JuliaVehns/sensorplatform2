package au.carrsq.sensorplatform.Models;

import org.threeten.bp.Instant;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.ZoneId;

/**
 * Created by julia on 05.05.2017.
 */

public class PhoneInteractionDataVector {

    public String code_id;
    public long timestamp;
    public String dateTime;
    public String interaction;
    public int sync;

    public PhoneInteractionDataVector(String code_id, String interaction) {
        this.code_id = code_id;
        this.interaction = interaction;
        setTimestamp(System.currentTimeMillis()/1000);
        setDateTime();
        this.sync = 0;

    }

    public void setDateTime() {
        Instant now = Instant.now();
        this.dateTime = LocalDateTime.ofInstant(now, ZoneId.systemDefault()).toString();
    }

    public String toString() {
        return "code_id: " + code_id + ", timestamp: " + timestamp + ", datetime: " + dateTime + ", interaction: " + interaction;
    }

    public String getCode_id() {
        return code_id;
    }

    public void setCode_id(String code_id) {
        this.code_id = code_id;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getDateTime() {
        return dateTime;
    }

    public String getInteraction() {
        return interaction;
    }

    public void setInteraction(String interaction) {
        this.interaction = interaction;
    }

    public int getSync() {
        return sync;
    }

    public void setSync(int sync) {
        this.sync = sync;
    }
}
