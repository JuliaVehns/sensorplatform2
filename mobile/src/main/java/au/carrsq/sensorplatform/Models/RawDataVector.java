package au.carrsq.sensorplatform.Models;

import org.threeten.bp.Instant;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.ZoneId;

/**
 * Each instance of this class contains the raw data values of a given time interval
 */
public class RawDataVector {

    public double latitude;
    public double longitude;
    public double speedCalculated = 0;
    public double speedLocationManager = 0;
    private String code_id;
    private long timestamp;
    private String dateTime;
    private double accelerationX;
    private double accelerationY;
    private double accelerationZ;
    private double rawAccelerationX;
    private double rawAccelerationY;
    private double rawAccelerationZ;
    private double gravityX;
    private double gravityY;
    private double gravityZ;
    private float gyroscopeX;
    private float gyroscopeY;
    private float gyroscopeZ;
    private double rotationX;
    private double rotationY;
    private double rotationZ;
    private float[] rotationMatrix;
    private double rotationXRad;
    private double rotationYRad;
    private double rotationZRad;

    //MVN-Algorithm
    private double jerk;
    private double steer;
    private double gacc;
    private double gbea;
    private double magneticFieldX;
    private double magneticFieldY;
    private double magneticFieldZ;
    private int sync;
    public RawDataVector() {
        setDateTime();
        setTimestamp(System.currentTimeMillis()/1000);
        this.sync = 0;
    }

    public double getSteer() {
        return steer;
    }

    public void setSteer(double steer) {
        this.steer = steer;
    }

    public double getJerk() {
        return jerk;
    }

    public void setJerk(double jerk) {
        this.jerk = jerk;
    }

    public double getGacc() {
        return gacc;
    }

    public void setGacc(double gacc) {
        this.gacc = gacc;
    }

    public double getGbea() {
        return gbea;
    }

    public void setGbea(double gbea) {
        this.gbea = gbea;
    }

   /* public double getSteeringAngle() {
        return steeringAngle;
    }

    public void setSteeringAngle(double steeringAngle) {
        this.steeringAngle = steeringAngle;
    }*/

    public void setDateTime() {
        Instant now = Instant.now();
        this.dateTime = LocalDateTime.ofInstant(now, ZoneId.systemDefault()).toString();
    }

    public void setCodeId(String c_id) {
        this.code_id = c_id;
    }

    public void setAcc(double x, double y, double z) {
        this.accelerationX = x;
        this.accelerationY = y;
        this.accelerationZ = z;
    }

    public void setGravity(double x, double y, double z) {
        this.gravityX = x;
        this.gravityY = y;
        this.gravityZ = z;
    }

    public void setGyroscope(float x, float y, float z) {
        this.gyroscopeX = x;
        this.gyroscopeY = y;
        this.gyroscopeZ = z;
    }

    public void setRawAcc(double x, double y, double z) {
        this.rawAccelerationX = x;
        this.rawAccelerationY = y;
        this.rawAccelerationZ = z;
    }

    public void setRot(double x, double y, double z) {
        this.rotationX = x;
        this.rotationY = y;
        this.rotationZ = z;
    }

    public void setRotRad(double x, double y, double z) {
        this.rotationXRad = x;
        this.rotationYRad = y;
        this.rotationZRad = z;
    }

    public void setLocation(double lat, double lon) {
        this.latitude = lat;
        this.longitude = lon;
    }

    public String getCode_id() {
        return code_id;
    }

    public void setCode_id(String code_id) {
        this.code_id = code_id;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long time) {
        this.timestamp = time;
    }

    public String getDateTime() {
        return dateTime;
    }

    public double getAccelerationX() {
        return accelerationX;
    }

    public double getAccelerationY() {
        return accelerationY;
    }

    public double getAccelerationZ() {
        return accelerationZ;
    }

    public double getRawAccelerationX() {
        return rawAccelerationX;
    }

    public double getRawAccelerationY() {
        return rawAccelerationY;
    }

    public double getRawAccelerationZ() {
        return rawAccelerationZ;
    }

    public double getGravityX() {
        return gravityX;
    }

    public double getGravityY() {
        return gravityY;
    }

    public double getGravityZ() {
        return gravityZ;
    }

    public double getGyroscopeX() {
        return gyroscopeX;
    }

    public double getGyroscopeY() {
        return gyroscopeY;
    }

    public double getGyroscopeZ() {
        return gyroscopeZ;
    }

    public double getRotationX() {
        return rotationX;
    }

    public double getRotationY() {
        return rotationY;
    }

    public double getRotationZ() {
        return rotationZ;
    }

    public double getRotationXRad() {
        return rotationXRad;
    }

    public double getRotationYRad() {
        return rotationYRad;
    }

    public double getLatitude() {
        return latitude;
    }



    public double getLongitude() {
        return longitude;
    }

    public double getSpeedCalculated() {
        return speedCalculated;
    }

    public void setSpeedCalculated(double s) {
        this.speedCalculated = s;
    }

    public Double getSpeedLocationManager() {
        return speedLocationManager;
    }

    public void setSpeedLocationManager(double speedLocationManager) {
        this.speedLocationManager = speedLocationManager;
    }

    public float[] getRotationMatrix() {
        return rotationMatrix;
    }

    public void setRotationMatrix(float[] matrix) {
        this.rotationMatrix = matrix;
    }

    public double getRotationZRad() {
        return rotationZRad;
    }

    public int getSync() {
        return sync;
    }

    public void setSync(int sync) {
        this.sync = sync;
    }

    public void setMagneticField(double magneticFieldX, double magneticFieldY, double magneticFieldZ) {
        this.magneticFieldX = magneticFieldX;
        this.magneticFieldY = magneticFieldY;
        this.magneticFieldZ = magneticFieldZ;
    }

    public double getMagneticFieldX() {
        return magneticFieldX;
    }

    public void setMagneticFieldX(double magneticFieldX) {
        this.magneticFieldX = magneticFieldX;
    }

    public double getMagneticFieldY() {
        return magneticFieldY;
    }

    public void setMagneticFieldY(double magneticFieldY) {
        this.magneticFieldY = magneticFieldY;
    }

    public double getMagneticFieldZ() {
        return magneticFieldZ;
    }

    public void setMagneticFieldZ(double magneticFieldZ) {
        this.magneticFieldZ = magneticFieldZ;
    }

    @Override
    public String toString() {
        return "code_id: " + code_id + ", timestamp: " + timestamp + ", datetime: " + dateTime + ", accelerationX: " + accelerationX + ", accelerationY: " + accelerationY + ", accelerationZ: " + accelerationZ + ", rawAccelerationX: " + rawAccelerationX + ", rawAccelerationY: " + rawAccelerationY + ", rawAccelerationZ: " + rawAccelerationZ + ", gravityX: " + gravityX + ", gravityY: " + gravityY + ", gravityZ: " + gravityZ + ", gyroscopeX: " + gyroscopeX + ", gyroscopeY: " + gyroscopeY + ", gyroscopeZ: " + gyroscopeZ +
                ", rotationX: " + rotationX + ", rotationY: " + rotationY + ", rotationZ: " + rotationZ + ", rotationXRad: " + rotationXRad + ", rotationYRad: " + rotationYRad + ", rotationZRad: " + rotationZRad + ", latitude: " + latitude + ", longitude: " + longitude + ", speedCalculated: " + speedCalculated + ", speedLocationManager: " + speedLocationManager + ", jerk: " + jerk + ", magneticFieldX: " + magneticFieldX + ", magneticFieldY: " + magneticFieldY + ", magneticFieldZ: " + magneticFieldZ + ", gacc: " + gacc + ", gbea: " + gbea;
    }

    public String toCSVString() {
        return code_id + ";" + timestamp + ";" + dateTime + ";" + accelerationX + ";" + accelerationY + ";" + accelerationZ + ";" + rawAccelerationX + ";" + rawAccelerationY + ";" + rawAccelerationZ + ";" +
                rotationXRad + ";" + rotationYRad + ";" + rotationZRad + ";" + latitude + ";" + longitude + ";" + speedCalculated;
    }


}
