package au.carrsq.sensorplatform.Models;


import org.threeten.bp.Instant;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.ZoneId;

import java.util.ArrayList;
import java.util.List;

public class EventVector {

    public String code_id;
    public String dateTime;
    private LEVEL level;
    private long timestamp;
    private String eventDescription;
    private double value;
    private double extraValue;
    private String videoFront;
    private String videoBack;
    private int sync;
    public EventVector(String c_id, LEVEL level, long time, String event, double value) {
        setDateTime();
        this.code_id = c_id;
        this.level = level;
        //this.timestamp = time;
        this.eventDescription = event;
        this.value = value;
        this.sync = 0;
        setTimestamp(System.currentTimeMillis()/1000);
    }

    public static List<LEVEL> convertToLevels(int[] int_levels) {
        List<LEVEL> levels = new ArrayList<>();

        for (int i = 0; i < int_levels.length; i++) {
            LEVEL l = EventVector.LEVEL.values()[int_levels[i]];
            levels.add(l);
        }

        return levels;
    }

    /*public EventVector(String c_id, LEVEL level, long time, String event, double value, double extra) {
        setDateTime();
        this.code_id = c_id;
        this.level = level;
        this.timestamp = time;
        this.eventDescription = event;
        this.value = value;
        this.extraValue = extra;
        this.sync = 0;
        setTimestamp(System.currentTimeMillis()/1000);
    }*/

    public int getSync() {
        return sync;
    }

    public void setSync(int sync) {
        this.sync = sync;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime() {
        Instant now = Instant.now();
        this.dateTime = LocalDateTime.ofInstant(now, ZoneId.systemDefault()).toString();
    }

    public String getCode_id() {
        return code_id;
    }

    public void setCode_id(String code_id) {
        this.code_id = code_id;
    }

    public String getVideoFront() {
        return videoFront;
    }

    public String getVideoBack() {
        return videoBack;
    }

    public void setVideoNames(long timestamp) {

        this.videoFront = "front-" + timestamp + ".avi";
        this.videoBack = "back-" + timestamp + ".avi";
    }

    public LEVEL getLevel() {
        return level;
    }

    public String getLevelString() {
        if (this.level == LEVEL.LOW_RISK)
            return "LOW_RISK";
        if (this.level == LEVEL.MEDIUM_RISK)
            return "MEDIUM_RISK";
        if (this.level == LEVEL.HIGH_RISK)
            return "HIGH_RISK";

        return "-";
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public double getExtraValue() {
        return extraValue;
    }

    public void setExtraValue(double extraValue) {
        this.extraValue = extraValue;
    }

    public String toString() {
        return "code_id: " + code_id + ", timestamp: " + timestamp + ", description: " + eventDescription + ", value: " + value + ", extra: " + extraValue + ", videofront: " + videoFront + ", videoback: " + videoBack;
    }

    public String toCSVString() {
        String level = getLevelString();
        return code_id + ";" + timestamp + ";" + level + ";" + eventDescription + ";" + value + ";" + extraValue + ";" + videoFront + ";" + videoBack;
    }

    public boolean isIncludedInLevel(int[] int_levels) {
        if (level == LEVEL.DEBUG || int_levels == null)
            return false;

        for (int i = 0; i < int_levels.length; i++) {
            LEVEL l = EventVector.LEVEL.values()[int_levels[i]];
            if (level == l)
                return true;
            if (l == LEVEL.ALL)
                return true;
        }

        return false;
    }

    public enum LEVEL {DEBUG, ALL, LOW_RISK, MEDIUM_RISK, HIGH_RISK}
}
