package au.carrsq.sensorplatform.Core;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.util.Log;

import java.io.File;

import au.carrsq.sensorplatform.Utilities.IO;

// When the device is at rest, the output of the magnaticField sensor should be identical to that of the accelerometer.
public class MagnaticFieldProvider extends SensorProvider {
    private Sensor magnaticFieldSensor;

    private double[] magnaticField = new double[3];
    private int WINDOW = 25;


    public MagnaticFieldProvider(Context a, SensorModule m) {
        super(a, m);
    }

    public void start() {
        Log.d("Geomagnetic Field ", "Started");
        super.start();
        if (sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD) != null) {
            magnaticFieldSensor = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
            WINDOW = (Preferences.getRawDataDelay(setting_prefs) / Preferences.getMagneticFieldDelay(setting_prefs));
            sensorManager.registerListener(this, magnaticFieldSensor, Preferences.getMagneticFieldDelay(setting_prefs));
        }else{
            File output = new File(IO.baseDir + File.separator + System.currentTimeMillis() / 1000 + "_TYPE_MAGNETIC_FIELD.txt");
            IO.writeFile(output,"TYPE_MAGNETIC_FIELD is not available");
        }

    }

    public void stop() {
        super.stop();
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        magnaticField[0] = event.values[0];
        magnaticField[1] = event.values[1];
        magnaticField[2] = event.values[2];

        sensorCallback.onMagneticFieldData(magnaticField);
    }

}
