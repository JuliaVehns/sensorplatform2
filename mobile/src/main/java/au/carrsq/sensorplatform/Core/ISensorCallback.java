package au.carrsq.sensorplatform.Core;


public interface ISensorCallback {
    void onAccelerometerData(double[] values);

    void onGravityData(double[] values);

    void onGyroscopeData(float[] values);

    // we need float to store the rotation matrix
    void onRotationData(float[][] values);

    void onLocationData(double lat, double lon, double speedLocationManager);


    void onMagneticFieldData(double[] magnaticField);
}
