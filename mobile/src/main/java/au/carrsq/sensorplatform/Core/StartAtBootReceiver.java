package au.carrsq.sensorplatform.Core;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.List;

import au.carrsq.sensorplatform.R;

import static au.carrsq.sensorplatform.Utilities.ThreadPool.finish;

/**
 * Created by julia on 26.05.2017.
 */

public class StartAtBootReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        // @TODO: remove this waiting for debugger
        android.os.Debug.waitForDebugger();

        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            SharedPreferences studyPrefs = context.getSharedPreferences(context.getString(R.string.study_preferences_key), Context.MODE_PRIVATE);
            String code_id = studyPrefs.getString("code_id", "");


            if (code_id != null && code_id != "") {
                Log.d("START_BOOT", code_id);
                Intent in = new Intent(context, MainActivity.class);
                in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                in.putExtra("reboot", true);
                context.startActivity(in);
            }
        }
    }
}
