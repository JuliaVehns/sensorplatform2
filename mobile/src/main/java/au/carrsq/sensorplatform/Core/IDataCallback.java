package au.carrsq.sensorplatform.Core;

import au.carrsq.sensorplatform.Models.EventVector;
import au.carrsq.sensorplatform.Models.RawDataVector;

public interface IDataCallback {

    void onRawData(RawDataVector dv);

    void onEventData(EventVector ev);
}
