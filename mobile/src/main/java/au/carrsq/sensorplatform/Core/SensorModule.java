package au.carrsq.sensorplatform.Core;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import au.carrsq.sensorplatform.Database.SqliteDataProvider;
import au.carrsq.sensorplatform.Models.EventVector;
import au.carrsq.sensorplatform.Models.RawDataVector;
import au.carrsq.sensorplatform.R;

/**
 * This class is the central unit of data collection (except image data).
 * It knows all the Sensor Provider classes and collects raw data values in one RawDataVector.
 */

public class SensorModule implements ISensorCallback, IEventCallback {
    private SharedPreferences setting_prefs;

    /* Callback implemented by SensorPlatformService */
    private IDataCallback callback;

    /* SensorProvider for acceleration data */
    private SensorProvider accelerometer;

    /**
     * SensorProvider for gravity data
     */
    private GravityProvider gravity;
    /**
     * SensorProvider for gyroscope data
     */
    private GyroscopeProvider gyroscope;
    /**
     * SensorProvider for orientation data
     */
    private SensorProvider orientation;

    /**
     * SensorProvider for magnetic field data
     */
    private SensorProvider magnetic_field;
    /**
     * PositionProvider to collect geolocation
     */
    private PositionProvider location;

    /**
     * List of all currently running SensorProviders
     */
    private List<DataProvider> activeProviders;
    /**
     * Vector containing the most recent raw data
     */
    private RawDataVector current;
    /**
     * A list containing the last BUFFERSIZE DataVectors
     */
    private ArrayList<RawDataVector> dataBuffer;
    /**
     * Indicator if a SensorProvider is currently active
     */
    private boolean sensing = false;
    /**
     * The EventProcessor for Driving Behaviour
     */
    // private DrivingBehaviourProcessor drivingBehProc;
    /**
     * The size of the dataBuffer
     */
    private final int BUFFERSIZE = 100;
    /**
     * int identifiers for 'non-Android sensors'
     */
    private final int GPS_IDENTIFIER = 100;
    private final int WEATHER_IDENTIFIER = 101;
//    private final int OBD_IDENTIFIER = 102;

    private String code_id;

    private Context app;

    private long lastTimeLocataionUpdate = 0;
    private long timeDifferenceGPSUpdate = 1;
    private RawDataVector lastGPSDataVector = new RawDataVector();
    RawDataVector last;
    private boolean locationUpdateForRotation = false;
    double temp_rotationXRad = 0.0;
    double temp_rotationYRad = 0.0;
    double temp_rotationZRad = 0.0;

    ContentResolver mContentResolver;


    public SensorModule(IDataCallback callback, Context app, Context applicationContext) {
        setting_prefs = app.getSharedPreferences(app.getString(R.string.settings_preferences_key), Context.MODE_PRIVATE);
        this.callback = callback;
        this.app = app;
        mContentResolver = applicationContext.getContentResolver();

        refresh();
    }

    public void refresh() {
        activeProviders = new ArrayList<>();

        accelerometer = new AccelerometerProvider(app, this);
        gravity = new GravityProvider(app, this);
        gyroscope = new GyroscopeProvider(app, this);
        orientation = new OrientationProvider(app, this);
        magnetic_field = new MagnaticFieldProvider(app, this);
        location = new PositionProvider(app, this);
        //drivingBehProc = new DrivingBehaviourProcessor(this, app);

        current = new RawDataVector();
        current.setTimestamp(System.currentTimeMillis() / 1000);
        current.setDateTime();
        dataBuffer = new ArrayList<>();
    }

    public void startSensing(DataType type) {
        if (!sensing) {
            int sampling = Preferences.getRawDataDelay(setting_prefs);
            aggregateData(sampling);
            sensing = true;
        }

        int t = getSensorTypeFromDataType(type);

        if (t == Sensor.TYPE_ACCELEROMETER && !activeProviders.contains(accelerometer)) {
            accelerometer.start();
            activeProviders.add(accelerometer);
            Log.d("START", "starting accelerometer");
        }

        if (t == Sensor.TYPE_GRAVITY && !activeProviders.contains(gravity)) {
            gravity.start();
            activeProviders.add(gravity);
            Log.d("START", "starting gravity");
        }
        if (t == Sensor.TYPE_GYROSCOPE && !activeProviders.contains(gyroscope)) {
            gyroscope.start();
            activeProviders.add(gyroscope);
            Log.d("START", "starting gyroscope");
        }

        if (t == Sensor.TYPE_ROTATION_VECTOR && !activeProviders.contains(orientation)) {
            orientation.start();
            activeProviders.add(orientation);
        }

        if (t == Sensor.TYPE_MAGNETIC_FIELD && !activeProviders.contains(magnetic_field)) {
            magnetic_field.start();
            activeProviders.add(magnetic_field);
        }

        if (t == GPS_IDENTIFIER && !activeProviders.contains(location)) {
            location.start();
            activeProviders.add(location);
            // orientation is needed for road detection
            if (!activeProviders.contains(orientation)) {
                orientation.start();
                activeProviders.add(orientation);
            }
        }
    }

    /**
     * This method stops the sensor after checking that no active subscriptions depend on it
     *
     * @param type: The unsubscribed DataType
     */
    public void stopSensing(DataType type) {
        int t = getSensorTypeFromDataType(type);

        Log.d("STOPPED", type + "");

        if (t == Sensor.TYPE_ACCELEROMETER) {
            accelerometer.stop();
            if (activeProviders.contains(accelerometer))
                activeProviders.remove(accelerometer);

        } else if (t == Sensor.TYPE_GRAVITY) {
            gravity.stop();
            if (activeProviders.contains(gravity))
                activeProviders.remove(gravity);
        } else if (t == Sensor.TYPE_GYROSCOPE) {
            gyroscope.stop();
            if (activeProviders.contains(gyroscope))
                activeProviders.remove(gyroscope);

        } else if (t == Sensor.TYPE_ROTATION_VECTOR) {
            orientation.stop();
            if (activeProviders.contains(orientation))
                activeProviders.remove(orientation);

        } else if (t == GPS_IDENTIFIER) {
            location.stop();
            orientation.stop();
            if (activeProviders.contains(location))
                activeProviders.remove(location);
            if (activeProviders.contains(orientation))
                activeProviders.remove(orientation);

        } else if (t == Sensor.TYPE_MAGNETIC_FIELD) {
            magnetic_field.stop();
            if (activeProviders.contains(magnetic_field)) {
                activeProviders.remove(magnetic_field);
            }
        }
        Log.d("STOPPED", activeProviders.size() + "");
        if (activeProviders.size() == 0) {
            sensing = false;
        }
    }

    public void stopAll() {
        activeProviders.clear();
        sensing = false;
    }

    private void aggregateData(final int ms) {
        last = current;
        current = new RawDataVector();
        /**
         * add last recorded RawDataVector to Buffer
         * init new RawDataVector with previous values to prevent empty entries;
         */
        if (last != null) {
            dataBuffer.add(last);
            current.setCodeId(code_id);
            current.setLocation(last.latitude, last.longitude);
            current.setAcc(0, 0, 0);
            current.setRawAcc(0, 0, 0);
            current.setGravity(0, 0, 0);
            current.setGyroscope(0, 0, 0);
            current.setRawAcc(0, 0, 0);
            current.setRot(last.getRotationX(), last.getRotationY(), last.getRotationZ());
            current.setRotationMatrix(last.getRotationMatrix());
            current.setRotRad(last.getRotationXRad(), last.getRotationYRad(), last.getRotationZRad());
            current.setSpeedCalculated(last.speedCalculated);
            current.setSpeedLocationManager(last.speedLocationManager);
            current.setJerk(last.getJerk());
            current.setSteer(last.getSteer());
            current.setGacc(0.0);
            current.setGbea(0.0);
            current.setMagneticFieldX(last.getMagneticFieldX());
            current.setMagneticFieldY(last.getMagneticFieldY());
            current.setMagneticFieldZ(last.getMagneticFieldZ());
            current.setSync(0);
        }
        /**
         * only store last BUFFERSIZE DataVectors
         */
        if (dataBuffer.size() > BUFFERSIZE) {
            dataBuffer.remove(0);
        }

        /**
         * report raw data after defined delay
         */
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                if (current != null) {
                    current.setTimestamp(System.currentTimeMillis() / 1000);
                    current.setDateTime();
                    startEventProcessing();
                    if (ActiveSubscriptions.rawActive()) {
                        //System.out.println("Data line: "+ current.toString());
                        callback.onRawData(current);
                    }
                }
                if (sensing) {
                    aggregateData(ms);
                }
            }
        }, ms);
    }

    private void startEventProcessing() {
        if (ActiveSubscriptions.drivingBehaviourActive()) {
            // drivingBehProc.processData(dataBuffer);
        }
    }

    public void clearDataBuffer() {
        dataBuffer.clear();
    }


    List<double[]> lastValues = new ArrayList<>();

    /**
     * This method is the accelerometer callback function.
     * It receives raw data values and stores them in the current RawDataVector
     *
     * @param dataValues: the values sensed by the accelerometer
     */
    @Override
    public void onAccelerometerData(double[] dataValues) {
        /**
         * Replace the current value only if the new value is more extreme.
         * This guarantees that the same value is stored independent of the <code>RawDataSampling</code> setting.
         */
        if (Math.abs(dataValues[2]) > Math.abs(current.getAccelerationX()))
            current.setAcc(dataValues[0], dataValues[1], dataValues[2]);
        //calculating jerk
        current.setJerk(calculateJerk(dataValues[0], dataValues[1], dataValues[2], last.getAccelerationX(), last.getRawAccelerationY(), last.getRawAccelerationZ()));

        if (current.getAccelerationY() == 0)
            current.setRawAcc(dataValues[3], dataValues[4], dataValues[5]);
        else
            current.setRawAcc((current.getRawAccelerationX() + dataValues[3]) / 2, (current.getRawAccelerationY() + dataValues[4]) / 2, (current.getRawAccelerationZ() + dataValues[5]) / 2);
        /**
         * Do event processing on the raw accelerometer data
         */
        lastValues.add(dataValues);
        if (lastValues.size() > 20) {
            lastValues.remove(0);
        }
        //drivingBehProc.checkForHardAccRaw(lastValues);

    }


    @Override
    public void onGravityData(double[] dataValues) {
        /**
         * Replace the current value only if the new value is more extreme.
         * This guarantees that the same value is stored independent of the <code>RawDataSampling</code> setting.
         */
        if (Math.abs(dataValues[2]) > Math.abs(current.getGravityZ()))
            current.setGravity(dataValues[0], dataValues[1], dataValues[2]);

    }

    @Override
    public void onGyroscopeData(float[] dataValues) {
        current.setGyroscope(dataValues[0], dataValues[1], dataValues[2]);

    }

    @Override
    public void onRotationData(float[][] values) {
        current.setRot(values[0][0], values[0][1], values[0][2]); //euler values in degree
        current.setRotationMatrix(values[1]);
        // order is y,z,x coming from the OrientationProvider
        current.setRotRad(values[2][2], values[2][0], values[2][1]);
        if(locationUpdateForRotation){
            temp_rotationXRad = current.getRotationXRad();
            temp_rotationYRad = current.getRotationYRad();
            temp_rotationZRad = current.getRotationZRad();
            locationUpdateForRotation = false;
        }
    }

    @Override
    public void onMagneticFieldData(double[] values) {
        current.setMagneticField(values[0], values[1], values[2]);
        current.setSteer(calculateSteer(values[0]));
    }



    @Override
    public void onLocationData(double lat, double lon, double speedLocationManager) {
        locationUpdateForRotation = true;
        timeDifferenceGPSUpdate = System.currentTimeMillis() / 1000 - lastTimeLocataionUpdate;
        lastTimeLocataionUpdate = System.currentTimeMillis() / 1000;

        lastGPSDataVector = new RawDataVector();
        lastGPSDataVector.setTimestamp(current.getTimestamp());
        lastGPSDataVector.setSpeedLocationManager(current.getSpeedLocationManager());
        lastGPSDataVector.setSpeedCalculated(current.getSpeedCalculated());
        lastGPSDataVector.setLocation(current.getLatitude(), current.getLongitude());

        lastGPSDataVector.setCode_id(current.getCode_id());
        lastGPSDataVector.setRotRad(temp_rotationXRad,temp_rotationYRad,temp_rotationZRad);


        current.setLocation(lat, lon);
        //current.setSpeedCalculated(speedCalculated);
        current.setSpeedLocationManager(speedLocationManager * 3.6);

        updateDatabase();

    }

    public void updateDatabase() {
        double gbea = calculateGbea(current.getRotationZRad());
        double gacc = calculateGacc(current.getSpeedLocationManager());

        ContentValues contentValues = new ContentValues();
        contentValues.put("gacc", gacc);
        contentValues.put("gbea", gbea);

        if (lastGPSDataVector.getCode_id() != null) {
            int rows = mContentResolver.update(SqliteDataProvider.RAW_DATA_URI, contentValues, "code_id=? AND latitude=? AND longitude=?", new String[]{String.valueOf(lastGPSDataVector.getCode_id()), String.valueOf(lastGPSDataVector.getLatitude()), String.valueOf(lastGPSDataVector.getLongitude())});
        }
    }

    public double calculateJerk(double rawAccelerationX, double rawAccelerationY, double rawAccelerationZ, double rawAccelerationX2, double rawAccelerationY2, double rawAccelerationZ2) {
        double jerk = 0.0;
        if (rawAccelerationX != 0 && rawAccelerationY != 0 && rawAccelerationZ != 0) {
            double step = Math.sqrt((rawAccelerationX * rawAccelerationX) + (rawAccelerationY * rawAccelerationY) + (rawAccelerationZ * rawAccelerationZ));
            double step2 = Math.sqrt((rawAccelerationX2 * rawAccelerationX2) + (rawAccelerationY2 * rawAccelerationY2) + (rawAccelerationZ2 * rawAccelerationZ2));
            jerk = (step2 - step) / Preferences.getRawDataDelay(setting_prefs);
            if (Double.isInfinite(jerk)) {
//                System.out.println("jerk is infinite");
            }

        }
        return jerk;
    }

    public double calculateSteer(double magneticFieldX) {
        double steer = 0.0;
        if (magneticFieldX != 0 && last.getMagneticFieldX() != 0 && magneticFieldX != last.getMagneticFieldX()) {
            steer = (magneticFieldX - last.getMagneticFieldX()) / Preferences.getRawDataDelay(setting_prefs);
            if (Double.isInfinite(steer)) {
//                System.out.println("steer is infinite");
            }
        }
        return steer;

    }

    public double calculateGacc(double speed) {
        double gacc = 0.0;
        if (lastGPSDataVector.getCode_id() == null && speed != 0 && lastGPSDataVector.getSpeedLocationManager() != speed) {
            gacc = (0.0 - speed) / timeDifferenceGPSUpdate;
        } else {
            if (lastGPSDataVector.getSpeedLocationManager() != speed && timeDifferenceGPSUpdate != 0) {
                gacc = (speed - lastGPSDataVector.getSpeedLocationManager()) / timeDifferenceGPSUpdate;
                if (Double.isInfinite(gacc)) {
                    gacc = 0.0;
//                    System.out.println("gacc is infinite");
                }
            }
        }
        return gacc;
    }

    public double calculateGbea(double rotationZRad) {
        double gbea = 0.0;
        if (lastGPSDataVector.getCode_id() == null) {
            gbea = (0.0 - rotationZRad) / timeDifferenceGPSUpdate;
        } else {
            if (lastGPSDataVector.getRotationZRad() != rotationZRad && timeDifferenceGPSUpdate != 0) {
                gbea = (lastGPSDataVector.getRotationZRad() - rotationZRad) / timeDifferenceGPSUpdate;
            }
            if (Double.isInfinite(gbea)) {
//                System.out.println("gbea is infinite");
            }
        }
        return gbea;
    }

    /**
     * Hands the EventVector to the SensorPlatformService
     *
     * @param v: the EventVector containing the event
     */
    @Override
    public void onEventDetected(EventVector v) {
        callback.onEventData(v);
    }

    /**
     * Hands the EventVector to the SensorPlatformService after adding the current timestamp
     *
     * @param v: the EventVector containing the event
     */
    @Override
    public void onEventDetectedWithoutTimestamp(EventVector v) {
        if (current.getTimestamp() != 0)
            v.setTimestamp(current.getTimestamp());
        callback.onEventData(v);
    }

    /**
     * Helper function to convert a DataType to a Sensor
     *
     * @param t: the DataType to be converted
     * @return returns the according Sensor Type (e.g. Sensor.TYPE_ACCELEROMETER)
     */
    private int getSensorTypeFromDataType(DataType t) {
        switch (t) {
            case ACCELERATION_EVENT:
            case ACCELERATION_RAW:
                return Sensor.TYPE_ACCELEROMETER;
            case GRAVITY_EVENT:
                return Sensor.TYPE_GRAVITY;
            case GYROSCOPE_EVENT:
                return Sensor.TYPE_GYROSCOPE;
            case ROTATION_EVENT:
            case ROTATION_RAW:
                return Sensor.TYPE_ROTATION_VECTOR;
            case LIGHT:
                return Sensor.TYPE_LIGHT;
            case LOCATION_RAW:
            case LOCATION_EVENT:
                return GPS_IDENTIFIER;
            case WEATHER:
                return WEATHER_IDENTIFIER;
          /*  case OBD:
                return OBD_IDENTIFIER;*/
            case HEART_RATE:
                return Sensor.TYPE_HEART_RATE;
            case MAGNETIC_FIELD:
                return Sensor.TYPE_MAGNETIC_FIELD;
            default:
                return -1;
        }
    }

    public void setStudyParameters() {
        SharedPreferences studyPrefs = app.getSharedPreferences(app.getString(R.string.study_preferences_key), Context.MODE_PRIVATE);
        code_id = studyPrefs.getString("code_id", "");
    }
}
