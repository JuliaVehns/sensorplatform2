package au.carrsq.sensorplatform.Core;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashSet;
import java.util.Set;

import au.carrsq.sensorplatform.Models.EventVector;
import au.carrsq.sensorplatform.R;

/**
 * This class provides an interface to the res/xml/settings_preferences.xmlences.xml file
 */

public class Preferences {
    public static final String FREQUENCY_RAWDATA = "frequency_rawData";

    public static final String ACCELEROMETER_RAW = "accelerometer_raw";

    public static final String GRAVITY_RAW = "gravity_raw";

    public static final String GYROSCOPE_RAW = "gyroscope_raw";

    public static final String ROTATION_RAW = "rotation_raw";

    public static final String LOCATION_RAW = "location_raw";

    public static final String OSM = "map_active";

    public static final String MAGNATIC_FIELD = "magnetic_field_active";

    public static final String FREQUENCY_ACCELEROMETER = "frequency_accelerometer";

    public static final String ACCELEROMETER_THRESHOLD_NORMAL = "acceleration_threshold_normal";

    public static final String ACCELEROMETER_THRESHOLD_RISKY = "accelerometer_threshold_risky";

    public static final String ACCELEROMETER_THRESHOLD_DANGEROUS = "accelerometer_threshold_dangerous";


    public static final String FREQUENCY_GRAVITY = "frequency_gravity";

    public static final String FREQUENCY_MAGNETIC_FIELD = "frequency_magnetic_field";

    public static final String FREQUENCY_GYROSCPOPE = "frequency_gyroscope";


    public static final String FREQUENCY_ROTATION = "frequency_rotation";

    public static final String TURN_THRESHOLD_NORMAL = "rotation_threshold_normal";

    public static final String TURN_THRESHOLD_RISKY = "rotation_threshold_risky";

    public static final String TURN_THRESHOLD_DANGEROUS = "rotation_threshold_dangerous";


    public static final String OSM_REQUEST_RATE = "osmRequest_frequency";

    public static final String GPS_REQUEST_RATE = "gpsRequest_frequency";

    public static final String LOGGING_RAW = "logging_raw";

    public static final String LOGGING_EVENT = "logging_event";

    public static final String LOG_LEVEL = "log_level";

    public static final String ORIENTATION = "reverse_orientation";


    private static Context context = null;

    public static void setContext(Context app) {
        context = app.getApplicationContext();
    }


    public static boolean accelerometerActivated(SharedPreferences prefs) {
        boolean value = prefs.getBoolean(ACCELEROMETER_RAW, true);

        return value;
    }

    public static boolean gravityActivated(SharedPreferences prefs) {
        boolean value = prefs.getBoolean(GRAVITY_RAW, true);

        return value;
    }

    public static boolean gyroscopeActivated(SharedPreferences prefs) {
        boolean value = prefs.getBoolean(GYROSCOPE_RAW, true);

        return value;
    }

    public static boolean rotationActivated(SharedPreferences prefs) {
        boolean value = prefs.getBoolean(ROTATION_RAW, true);

        return value;
    }

    public static boolean locationActivated(SharedPreferences prefs) {
        boolean value = prefs.getBoolean(LOCATION_RAW, true);

        return value;
    }

    public static boolean osmActivated(SharedPreferences prefs) {
        boolean value = prefs.getBoolean(OSM, true);

        return value;
    }

    public static boolean magneticFieldActivated(SharedPreferences prefs) {
        boolean value = prefs.getBoolean(MAGNATIC_FIELD, true);

        return value;
    }

    public static int getRawDataDelay(SharedPreferences prefs) {
        String valueString = prefs.getString(FREQUENCY_RAWDATA, "100");

        int value = context.getResources().getInteger(R.integer.rawData_delay_default);

        try {
            value = Integer.valueOf(valueString);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (value <= 0) {
            value = context.getResources().getInteger(R.integer.rawData_delay_default);
        }

        return value;
    }


    /**
     * ACCELEROMETER
     */
    public static int getAccelerometerDelay(SharedPreferences prefs) {
        String valueString = prefs.getString(FREQUENCY_ACCELEROMETER, "100");

        int value = context.getResources().getInteger(R.integer.accelerometer_delay_default);

        try {
            value = Integer.valueOf(valueString);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (value <= 0) {
            value = context.getResources().getInteger(R.integer.accelerometer_delay_default);
        }

        return value;
    }

    /**
     * GRAVITY
     */
    public static int getGravityDelay(SharedPreferences prefs) {
        String valueString = prefs.getString(FREQUENCY_GRAVITY, "100");

        int value = context.getResources().getInteger(R.integer.gravity_delay_default);

        try {
            value = Integer.valueOf(valueString);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (value <= 0) {
            value = context.getResources().getInteger(R.integer.gravity_delay_default);
        }

        return value;
    }

    /**
     * Magnetic Field
     */
    public static int getMagneticFieldDelay(SharedPreferences prefs) {
        String valueString = prefs.getString(FREQUENCY_MAGNETIC_FIELD, "100");

        int value = context.getResources().getInteger(R.integer.magnetic_field_delay_default);

        try {
            value = Integer.valueOf(valueString);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (value <= 0) {
            value = context.getResources().getInteger(R.integer.magnetic_field_delay_default);
        }

        return value;
    }

    public static int getGyroscopeDelay(SharedPreferences prefs) {
        String valueString = prefs.getString(FREQUENCY_GYROSCPOPE, "100");

        int value = context.getResources().getInteger(R.integer.gyroscope_delay_default);

        try {
            value = Integer.valueOf(valueString);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (value <= 0) {
            value = context.getResources().getInteger(R.integer.gyroscope_delay_default);
        }

        return value;
    }

    public static double getNormalAccelerometerThreshold(SharedPreferences prefs) {
        String valueString = prefs.getString(ACCELEROMETER_THRESHOLD_NORMAL, "0.15");

        double value = Double.valueOf(context.getResources().getString(R.string.acceleration_threshold_normal_default));

        try {
            value = Double.valueOf(valueString) * 9.81; // convert to m/s^2
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (value <= 0) {
            value = Double.valueOf(context.getResources().getString(R.string.acceleration_threshold_normal_default)) * 9.81;
        }

        return value;
    }

    public static double getRiskyAccelerometerThreshold(SharedPreferences prefs) {
        String valueString = prefs.getString(ACCELEROMETER_THRESHOLD_RISKY, "0.25");

        double value = Double.valueOf(context.getResources().getString(R.string.acceleration_threshold_risky_default));

        try {
            value = Double.valueOf(valueString) * 9.81; // convert to m/s^2
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (value <= 0) {
            value = Double.valueOf(context.getResources().getString(R.string.acceleration_threshold_risky_default)) * 9.81;
        }

        return value;
    }

    public static double getDangerousAccelerometerThreshold(SharedPreferences prefs) {
        String valueString = prefs.getString(ACCELEROMETER_THRESHOLD_DANGEROUS, "0.35");

        double value = Double.valueOf(context.getResources().getString(R.string.acceleration_threshold_dangerous_default));

        try {
            value = Double.valueOf(valueString) * 9.81; // convert to m/s^2
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (value <= 0) {
            value = Double.valueOf(context.getResources().getString(R.string.rotation_threshold_dangerous_default)) * 9.81;
        }

        return value;
    }


    /**
     * ROTATION
     */
    public static int getOrientationDelay(SharedPreferences prefs) {
        String valueString = prefs.getString(FREQUENCY_ROTATION, "60000");

        int value = context.getResources().getInteger(R.integer.rotation_delay_default);

        try {
            value = Integer.valueOf(valueString);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (value <= 0) {
            value = context.getResources().getInteger(R.integer.rotation_delay_default);
        }

        return value;
    }

    public static double getNormalTurnThreshold(SharedPreferences prefs) {
        String valueString = prefs.getString(TURN_THRESHOLD_NORMAL, "0.45");

        double value = Double.valueOf(context.getResources().getString(R.string.rotation_threshold_normal_default));

        try {
            value = Double.valueOf(valueString);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (value <= 0) {
            value = Double.valueOf(context.getResources().getString(R.string.rotation_threshold_normal_default));
        }

        return value;
    }

    public static double getRiskyTurnThreshold(SharedPreferences prefs) {
        String valueString = prefs.getString(TURN_THRESHOLD_RISKY, "0.6");

        double value = Double.valueOf(context.getResources().getString(R.string.rotation_threshold_risky_default));

        try {
            value = Double.valueOf(valueString);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (value <= 0) {
            value = Double.valueOf(context.getResources().getString(R.string.rotation_threshold_risky_default));
        }

        return value;
    }

    public static double getDangerousTurnThreshold(SharedPreferences prefs) {
        String valueString = prefs.getString(TURN_THRESHOLD_DANGEROUS, "0.7");

        double value = Double.valueOf(context.getResources().getString(R.string.rotation_threshold_dangerous_default));

        try {
            value = Double.valueOf(valueString);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (value <= 0) {
            value = Double.valueOf(context.getResources().getString(R.string.rotation_threshold_dangerous_default));
        }

        return value;
    }


    /**
     * LOCATION
     */
    public static int getOSMRequestRate(SharedPreferences prefs) {
        String valueString = prefs.getString(OSM_REQUEST_RATE, "5000");

        int value = context.getResources().getInteger(R.integer.osmRequest_delay_default);

        try {
            value = Integer.valueOf(valueString);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (value <= 0) {
            value = context.getResources().getInteger(R.integer.osmRequest_delay_default);
        }

        return value;
    }

    public static int getGPSRequestRate(SharedPreferences prefs) {
        String valueString = prefs.getString(GPS_REQUEST_RATE, "1000");

        int value = context.getResources().getInteger(R.integer.gpsRequest_delay_default);

        try {
            value = Integer.valueOf(valueString);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (value <= 0) {
            value = context.getResources().getInteger(R.integer.gpsRequest_delay_default);
        }

        return value;
    }

    /**
     * LOGGING AND SURVEY
     */

    public static boolean rawLoggingActivated(SharedPreferences prefs) {
        boolean value = prefs.getBoolean(LOGGING_RAW, false);

        return value;
    }

    public static boolean eventLoggingActivated(SharedPreferences prefs) {
        boolean value = prefs.getBoolean(LOGGING_EVENT, false);

        return value;
    }

    public static int[] getLogLevel(SharedPreferences prefs) {
        Set<String> def = new HashSet<String>();
        def.add(EventVector.LEVEL.ALL.toString());

        Set<String> valueString = prefs.getStringSet(LOG_LEVEL, def);
        String[] a = valueString.toArray(new String[]{});

        int[] value = new int[a.length];

        for (int i = 0; i < a.length; i++) {
            int parsed;
            try {
                parsed = Integer.valueOf(a[i]);
                value[i] = parsed;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return value;
    }

    public static boolean isReverseOrientation(SharedPreferences prefs) {
        Boolean reverse = prefs.getBoolean(ORIENTATION, false);

        return reverse;
    }


}
