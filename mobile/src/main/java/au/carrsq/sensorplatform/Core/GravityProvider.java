package au.carrsq.sensorplatform.Core;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.util.Log;

import java.io.File;

import au.carrsq.sensorplatform.Utilities.IO;

// When the device is at rest, the output of the gravity sensor should be identical to that of the accelerometer.
public class GravityProvider extends SensorProvider {
    private Sensor gravitySensor;

    private double[] gravity = new double[3];
    private int WINDOW = 25;


    public GravityProvider(Context a, SensorModule m) {
        super(a, m);
    }

    public void start() {
        Log.d("Gravity", "Started");
        super.start();
        if (sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY) != null) {
            gravitySensor = sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
            WINDOW = (Preferences.getRawDataDelay(setting_prefs) / Preferences.getGravityDelay(setting_prefs));
            sensorManager.registerListener(this, gravitySensor, Preferences.getGravityDelay(setting_prefs));
        }else{
            File output = new File(IO.baseDir + File.separator + System.currentTimeMillis() / 1000 + "_TYPE_GRAVITY.txt");
            IO.writeFile(output,"TYPE_GRAVITY is not available");
        }

    }

    public void stop() {
        super.stop();
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        gravity[0] = event.values[0];
        gravity[1] = event.values[1];
        gravity[2] = event.values[2];

        sensorCallback.onGravityData(gravity);
    }

}
