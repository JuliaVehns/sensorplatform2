package au.carrsq.sensorplatform.Core;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AppOpsManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.ContentObserver;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import au.carrsq.sensorplatform.Database.SqliteDataObserver;
import au.carrsq.sensorplatform.Database.WifiWatcher;
import au.carrsq.sensorplatform.R;
import au.carrsq.sensorplatform.UI.AppFragment;
import au.carrsq.sensorplatform.UI.StartFragment;
import au.carrsq.sensorplatform.Utilities.NotificationListener;
import au.carrsq.sensorplatform.Utilities.PermissionManager;
import au.carrsq.sensorplatform.Utilities.PhoneInteractionService;
import au.carrsq.sensorplatform.Utilities.UncaughtExceptionHandler;

//import android.nfc.NfcAdapter;


public class MainActivity extends AppCompatActivity {

    public static boolean mBound = false;
    public static boolean started = false;

    // Service checking Wifi connection
    public static WifiWatcher wifiWatcher;

    /**
     * Load native libraries for OpenCV and image processing
     */
    static {
        try {
            System.loadLibrary("opencv_java3");
            System.loadLibrary("imgProc");
        } catch (UnsatisfiedLinkError e) {
            Log.d("APPLICATION INIT", "Unsatisfied Link error: " + e.toString());
        }
    }

    /**
     * Fragments
     */
    StartFragment startFragment;
    AppFragment appFragment;
    SensorPlatformService sPS;
    SharedPreferences sensor_prefs, setting_prefs, study_prefs;
    private boolean inStartFragment, inAppFragment = false;

    private boolean studySetupComplete = false;
    private boolean setupStarted = false;

    // Observes changes in sqlite db
   /* private SqliteDataObserver sqliteRawDataObserver;
    private SqliteDataObserver sqliteEventDataObserver;*/

    // observes user interaction with phone
    private ContentObserver notificationContentObserver;

    public static final String ACCOUNT_TYPE = "au.carrsq.sensorplatform.Synchronization";
    public static final String AUTHORITY = "au.carrsq.sensorplatform.Database.SqliteDataProvider.provider";
    private static final String TAG = "MainActivity";
    private static Account account;
    /**
     * The connection to the background service
     * Even if this connection disconnects, the service continues running
     */
    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get SensorPlatformService instance
            SensorPlatformService.LocalBinder binder = (SensorPlatformService.LocalBinder) service;
            sPS = binder.getService();
            mBound = true;

            sensorPlatformConected();


        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    public void sensorPlatformConected() {
        // after reboot, data measuring is starting itself
        if (getIntent().getBooleanExtra("reboot", false)) {
            startAutoMeasuringAfterReboot();
        }
    }

    /**
     * Start activity when the OS forwards an intent triggered by a NFC tag.
     * Prevents second activity to start if application is already running
     */
    public static void setupForegroundDispatch(final Activity activity) {
        final Intent intent = new Intent(activity.getApplicationContext(), activity.getClass());
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        final PendingIntent pendingIntent = PendingIntent.getActivity(activity.getApplicationContext(), 0, intent, 0);

//        IntentFilter[] filters = new IntentFilter[1];
//        String[][] techList = new String[][]{};

        // Notice that this is the same filter as in our manifest.
      /*  filters[0] = new IntentFilter();
        filters[0].addAction(NfcAdapter.ACTION_NDEF_DISCOVERED);
        filters[0].addCategory(Intent.CATEGORY_DEFAULT);
        try {
            filters[0].addDataType("text/plain");
        } catch (IntentFilter.MalformedMimeTypeException e) {
            throw new RuntimeException("Check your mime type.");
        }*/

        //adapter.enableForegroundDispatch(activity, pendingIntent, filters, techList);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // relevant for crash logging
        Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler());

        // make sure all the required permissions are granted
        PermissionManager.verifyPermissions(this);

        // populate the preference files
        setupPreferences();

        // sets up the wifi service
        wifiWatcher = new WifiWatcher();
        IntentFilter intentWifiFilter = new IntentFilter();
        intentWifiFilter.addAction(WifiManager.SUPPLICANT_CONNECTION_CHANGE_ACTION);
        intentWifiFilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        intentWifiFilter.addAction(SqliteDataObserver.ACTION_DATA_CHANGED);
        registerReceiver(wifiWatcher, intentWifiFilter);

        // Observer for local db changes
       /* sqliteRawDataObserver = new SqliteDataObserver(new Handler(), getApplicationContext());
        sqliteEventDataObserver = new SqliteDataObserver(new Handler(), getApplicationContext());
        getContentResolver().registerContentObserver(SqliteDataProvider.RAW_DATA_URI, true, sqliteRawDataObserver);
        getContentResolver().registerContentObserver(SqliteDataProvider.EVENT_DATA_URI, true, sqliteEventDataObserver);*/

        // set up dummy account for android sync adapter
        account = new Account("dummyaccount", ACCOUNT_TYPE);
        AccountManager accountManager = (AccountManager) this.getSystemService(ACCOUNT_SERVICE);
        accountManager.addAccountExplicitly(account, null, Bundle.EMPTY);
        ContentResolver.setSyncAutomatically(account, AUTHORITY, true);
        ContentResolver.setIsSyncable(account, AUTHORITY, 1);

        ContentResolver.addPeriodicSync(account, AUTHORITY, Bundle.EMPTY, 60);

        //checks if sqlite db has changed and triggers synchronisation
        //checkForDataChanges();

        // Service for observing user interaction
        ComponentName cn = new ComponentName(getApplicationContext(), NotificationListener.class);
        String flat = Settings.Secure.getString(getApplicationContext().getContentResolver(), "enabled_notification_listeners");
        final boolean enabled = flat != null && flat.contains(cn.flattenToString());
        if (!enabled) {
            requestNotifications();

        }

        // Check if this is the first start or just a re-instantiation
        if (savedInstanceState == null) {
            Log.d("CREATE", "New activity");
            if (getIntent().getBooleanExtra("reboot", false)) {
                Log.d("CREATE", "Service restart, binding it and changing fragment");
                Intent intent = new Intent(this, SensorPlatformService.class);
                bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
                startService(intent);
                goToAppFragment();

            } else if (!checkIfServiceRunning() || (!studySetupComplete && setupStarted)) {
                Log.d("CREATE", "Service not running, go to start");
                Intent intent = new Intent(this, SensorPlatformService.class);
                bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
                startService(intent);
                goToStartFragment(0, true);

            } /*else if (checkIfServiceRunning() && (studySetupComplete || !setupStarted)) { // new activity but service is already running
                started = true;
                Log.d("CREATE", "Service running, binding it and changing fragment");
                Intent intent = new Intent(this, SensorPlatformService.class);
                bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
                startService(intent);

                goToAppFragment();

            }*/

        } else { // activity was destroyed (e.g. for orientation change)
            started = savedInstanceState.getBoolean("started");

            if (!mBound) {
                Log.d("BINDING", "Rebinding service");
                Intent intent = new Intent(this, SensorPlatformService.class);
                bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
            }

            Log.d("RECREATE", "started:" + started + ", bound:" + mBound);

            // if the data collection was already started, switch to fragment that shows live data
            if (started)
                goToAppFragment();
        }

    }

    // make sure the preference files are filled with the right values
    public void setupPreferences() {
        sensor_prefs = this.getSharedPreferences(getString(R.string.sensor_preferences_key), Context.MODE_PRIVATE);
        sensor_prefs.edit().clear();
        if (sensor_prefs.getAll().isEmpty()) {
            PreferenceManager.setDefaultValues(this, getString(R.string.sensor_preferences_key), Context.MODE_PRIVATE, R.xml.sensor_preferences, true);
            sensor_prefs.edit().apply();
        } else {
            PreferenceManager.setDefaultValues(this, getString(R.string.sensor_preferences_key), Context.MODE_PRIVATE, R.xml.sensor_preferences, false);
        }

        setting_prefs = this.getSharedPreferences(getString(R.string.settings_preferences_key), Context.MODE_PRIVATE);
        setting_prefs.edit().clear();
        if (setting_prefs.getAll().isEmpty()) {
            PreferenceManager.setDefaultValues(this, getString(R.string.settings_preferences_key), Context.MODE_PRIVATE, R.xml.settings_preferences, true);
            setting_prefs.edit().apply();
        } else {
            PreferenceManager.setDefaultValues(this, getString(R.string.settings_preferences_key), Context.MODE_PRIVATE, R.xml.settings_preferences, false);
        }

        study_prefs = this.getSharedPreferences(getString(R.string.study_preferences_key), Context.MODE_PRIVATE);
        study_prefs.edit().clear();
        if (study_prefs.getAll().isEmpty()) {
            PreferenceManager.setDefaultValues(this, getString(R.string.study_preferences_key), Context.MODE_PRIVATE, R.xml.study_preferences, true);
            study_prefs.edit().apply();
        } else {
            PreferenceManager.setDefaultValues(this, getString(R.string.study_preferences_key), Context.MODE_PRIVATE, R.xml.study_preferences, false);
        }
    }

    /**
     * changes to the live data fragment and notifies service to start data collection
     */
    public void startMeasuring() {
        if (sPS != null) {
            goToAppFragment();

            // start logging interactions of user
            Intent phoneInteractionIntent = new Intent(getApplicationContext(), PhoneInteractionService.class);
            phoneInteractionIntent.addCategory(PhoneInteractionService.TAG);
            getActivity().startService(phoneInteractionIntent);

            Intent startIntent = new Intent(this, SensorPlatformService.class);
            startIntent.setAction("SERVICE_DATA_START");
            startService(startIntent);
            started = true;

            // trip start detection
            sPS.startWaitBehaviour();
            // start data collection immediately
            //sPS.subscribe();
        }

    }

    private void doUnbindService() {
        try {
            unbindService(mConnection);
            mBound = false;
        } catch (Exception e) {
            Log.e("MAIN", e.toString());
        }
    }

    public ServiceConnection getConnection() {
        return mConnection;
    }

    public SensorPlatformService getService() {
        return sPS;
    }

    public boolean checkIfServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (SensorPlatformService.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        // save whether the data collection was already started
        savedInstanceState.putBoolean("started", started);
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        Log.d("OnNewIntent", intent.getAction());
    }

    private void startAutoMeasuringAfterReboot() {
        SharedPreferences studyPrefs = getApplicationContext().getSharedPreferences(getApplicationContext().getString(R.string.study_preferences_key), Context.MODE_PRIVATE);
        String code_id = studyPrefs.getString("code_id", "");
        if (getIntent().getBooleanExtra("reboot", false) && code_id != "") {
            startMeasuring();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
       /* getContentResolver().registerContentObserver(ContentUris.withAppendedId(SqliteDataProvider.RAW_DATA_URI, 2), true, sqliteRawDataObserver);
        getContentResolver().registerContentObserver(ContentUris.withAppendedId(SqliteDataProvider.EVENT_DATA_URI, 4), true, sqliteEventDataObserver);*/

        /**
         * It's important that the activity is in the foreground (resumed). Otherwise
         * an IllegalStateException is thrown.
         */
       /* NfcAdapter adapter = NfcAdapter.getDefaultAdapter(this);
        if (adapter != null && adapter.isEnabled()) {
            setupForegroundDispatch(this, adapter);
        }*/

        Intent intent = new Intent(this, SensorPlatformService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        startService(intent);

//        handleSurveyIntent();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (checkIfServiceRunning()) {
            doUnbindService();
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        /*getContentResolver().unregisterContentObserver(sqliteRawDataObserver);
        getContentResolver().unregisterContentObserver(sqliteEventDataObserver);*/
        // Only kill the service if data collection was not yet started
        if (!studySetupComplete && checkIfServiceRunning() && !started && !SensorPlatformService.serviceRunning) {
            Intent intent = new Intent(this, SensorPlatformService.class);
            intent.setAction("SERVICE_STOP");
            stopService(intent);
            Log.d("On Destroy", "Killed Service");
        }
        unregisterReceiver(wifiWatcher);
    }

    @Override
    public void onBackPressed() {
        if (inStartFragment) {
            onDestroy();
            System.exit(0);
            return;
        } else if (inAppFragment) {
            goToStartFragment(0, false);
            return;
        }
        super.onBackPressed();
    }

    public void goToAppFragment() {
        this.appFragment = new AppFragment();
        changeFragment(this.appFragment, true, true, true);

        boolean reversed = Preferences.isReverseOrientation(setting_prefs);
        if (reversed)
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
        else
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        inAppFragment = true;
        studySetupComplete = true;
    }

    MainActivity getActivity() {
        return this;
    }

    /**
     * Changing to the StartFragment allows for a short delay to allow the service to shut down completely
     */
    public void goToStartFragment(int ms, final boolean forward) {
        startFragment = new StartFragment();

        Handler wait = new Handler();
        wait.postDelayed(new Runnable() {
            @Override
            public void run() {
                changeFragment(startFragment, true, true, forward);
                MainActivity that = getActivity();
                that.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
                inStartFragment = true;
            }
        }, ms);
        inStartFragment = true;

    }

    private void changeFragment(Fragment frag, boolean saveInBackstack, boolean animate, boolean forward) {
        // reset any fragment booleans
        inStartFragment = false;
        inAppFragment = false;
       /* inCameraFragment = false;*/

        String backStateName = ((Object) frag).getClass().getName();

        try {
            FragmentManager manager = getSupportFragmentManager();
            boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

            if (!fragmentPopped && manager.findFragmentByTag(backStateName) == null) {
                Log.w("FRAGMENT", "Create new Fragment: " + backStateName + ", " + frag);
                //fragment not in back stack, create it.
                FragmentTransaction transaction = manager.beginTransaction();

                if (animate) {
                    if (forward)
                        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
                    else
                        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
                }

                transaction.replace(R.id.fragment_container, frag, backStateName);

                if (saveInBackstack) {
                    transaction.addToBackStack(backStateName);
                }

                transaction.commit();

            } else {
                Log.w("FRAGMENT", "Already instantiated, popped back: " + backStateName);
                Fragment toShow = manager.findFragmentByTag(backStateName);
                /*FragmentTransaction transaction = manager.beginTransaction();
                if(forward)
                    transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
                else
                    transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
                transaction.show(toShow).commit();*/
                if (toShow.getView() != null)
                    toShow.getView().bringToFront();

            }
        } catch (IllegalStateException exception) {
            Log.w("FRAGMENT", "Unable to commit fragment, could be activity has been killed in background. " + exception.toString());
        }
    }


    private boolean isAccessGranted() {
        try {
            PackageManager packageManager = getPackageManager();
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(getPackageName(), 0);
            AppOpsManager appOpsManager = (AppOpsManager) getSystemService(Context.APP_OPS_SERVICE);
            int mode = 0;
            if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.KITKAT) {
                mode = appOpsManager.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS,
                        applicationInfo.uid, applicationInfo.packageName);
            }
            return (mode == AppOpsManager.MODE_ALLOWED);

        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    private void requestNotifications() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this)) {
            Intent overlay_p = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
            overlay_p.setData(Uri.parse("package:" + getPackageName()));
            startActivityForResult(overlay_p, 1337);

        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Intent ustats = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
                startActivityForResult(ustats, 1338);
            } else {
                Uri uri = Settings.System.CONTENT_URI;
                Log.d("URI", uri.toString());
                HandlerThread thread = new HandlerThread("MyHandlerThread");
                thread.start();
                Handler handler = new Handler(thread.getLooper());
                notificationContentObserver = new NotificationContentObserver(handler);
                getApplicationContext().getContentResolver().registerContentObserver(
                        uri,
                        false,
                        notificationContentObserver
                );

                Intent notifications_p = new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
                startActivityForResult(notifications_p, 1339);
            }

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1337) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && !isAccessGranted()) {
                Intent ustats = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
                startActivityForResult(ustats, 1338);
            }

        } else if (requestCode == 1338) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Settings.canDrawOverlays(this)) {
                Intent notifications_p = new Intent(Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS);
                startActivityForResult(notifications_p, 1339);
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Intent notifications_p = new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
                startActivityForResult(notifications_p, 1339);
            }

        } else if (requestCode == 1339) {
            //firstLayout.setVisibility(View.INVISIBLE);
            //setupLayout.setVisibility(View.VISIBLE);
            //next.setVisibility(View.INVISIBLE);
            //setup();
        }
    }

    class NotificationContentObserver extends ContentObserver {
        NotificationContentObserver(Handler h) {
            super(h);
        }

        @Override
        public boolean deliverSelfNotifications() {
            return true;
        }

        @Override
        public void onChange(boolean selfChange) {
            Log.d("NOTIFICATION_OBSERVER", "MyContentObserver.onChange(" + selfChange + ")");
            super.onChange(selfChange);

            ComponentName cn = new ComponentName(getApplicationContext(), NotificationListener.class);
            String flat = Settings.Secure.getString(getApplicationContext().getContentResolver(), "enabled_notification_listeners");
            final boolean enabled = flat != null && flat.contains(cn.flattenToString());
            if (enabled) {
                getApplicationContext().getContentResolver().unregisterContentObserver(notificationContentObserver);
            }
        }
    }
}
