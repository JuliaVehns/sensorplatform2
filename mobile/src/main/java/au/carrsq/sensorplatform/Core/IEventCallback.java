package au.carrsq.sensorplatform.Core;


import au.carrsq.sensorplatform.Models.EventVector;

public interface IEventCallback {
    void onEventDetected(EventVector v);

    void onEventDetectedWithoutTimestamp(EventVector v);
}
