package au.carrsq.sensorplatform.Core;


public enum DataType {
    /**
     *
     */
    ACCELERATION_RAW,
    /**
     *
     */
    ACCELERATION_EVENT,
    /**
     *
     */
    GRAVITY_EVENT,

    GYROSCOPE_EVENT,

    ROTATION_RAW,
    /**
     *
     */
    ROTATION_EVENT,
    /**
     *
     */
    LIGHT,
    /**
     *
     */
    LOCATION_RAW,
    /**
     *
     */
    LOCATION_EVENT,
    /**
     *
     */
    CAMERA_RAW,
    /**
     *
     */
    WEATHER,
    /**
     *
     */
    OBD,
    /**
     *
     */
    HEART_RATE,

    MAGNETIC_FIELD
}
