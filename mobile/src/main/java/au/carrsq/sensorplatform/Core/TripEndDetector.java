package au.carrsq.sensorplatform.Core;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import au.carrsq.sensorplatform.Models.RawDataVector;
import au.carrsq.sensorplatform.R;

/**
 * Analyzes Datavectors in real-time to detect a trip end.
 * Trip end is detected ...
 * A. ... if the OBD engine speedCalculated == 0
 * B. ... if the vehicle speedCalculated < 1 for 30 seconds
 */
public class TripEndDetector {
    private SharedPreferences sensor_prefs;

    private boolean counting = false;
    private Long firstStop = null;
    private int count = 0;

    private ITripDetectionCallback callback;

    public TripEndDetector(ITripDetectionCallback callback, Context c) {
        sensor_prefs = c.getSharedPreferences(c.getString(R.string.sensor_preferences_key), Context.MODE_PRIVATE);

        this.callback = callback;
    }


    public void checkForTripEnd(RawDataVector dv) {
        if(++count < 10)
            return;

        boolean location_active = Preferences.locationActivated(sensor_prefs);

        if(location_active && dv.latitude != 0 && dv.longitude != 0)
            checkInSpeed(dv);

    }

    private void checkInOBD(RawDataVector dv) {
        /*if(dv.obdRpm != null && dv.obdRpm == 0.0 && dv.obdSpeed == 0.0) {
            Log.d("TRIP END","Trip end in OBD");
            callback.onTripEnd();
        }*/
    }

    public void reset() {
        counting = false;
        firstStop = null;
        count = 0;
    }

    private void checkInSpeed(RawDataVector dv) {
        if((dv.speedCalculated < 1 && dv.speedCalculated >= 0) || (dv.speedLocationManager <1 && dv.speedLocationManager >=0)) {
            if(!counting) {
                firstStop = dv.getTimestamp();
                counting = true;
            } else {
                long duration = dv.getTimestamp() - firstStop;

                if(duration > 180) {
                    Log.d("TRIP END","Trip end in Speed");
                    callback.onTripEnd();
                }
            }
        } else {
            reset();

        }
    }
}
