package au.carrsq.sensorplatform.Core;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.util.Log;

import java.io.File;

import au.carrsq.sensorplatform.Utilities.IO;


public class GyroscopeProvider extends SensorProvider {
    private Sensor gyroscopeSensor;

    private double[] gyroscope = new double[3];
    private int WINDOW = 25;

    private static final float NS2S = 1.0f / 1000000000.0f;
    private final float[] deltaRotationVector = new float[4];
    private float timestamp;



    public GyroscopeProvider(Context a, SensorModule m) {
        super(a, m);
    }

    public void start() {
        Log.d("Gyroscope", "Started");
        super.start();

        if (sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE) != null) {
            gyroscopeSensor = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
            WINDOW = (Preferences.getRawDataDelay(setting_prefs) / Preferences.getGyroscopeDelay(setting_prefs));

            sensorManager.registerListener(this, gyroscopeSensor, Preferences.getGyroscopeDelay(setting_prefs));
        }else{
            File output = new File(IO.baseDir + File.separator + System.currentTimeMillis() / 1000 + "_TYPE_GYROSCOPE.txt");
            IO.writeFile(output,".TYPE_GYROSCOPE is not available");
        }

    }

    public void stop() {
        super.stop();
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float [] data = new float[3];
        data[0]= event.values[0];
        data[1]= event.values[1];
        data[2]= event.values[2];
        sensorCallback.onGyroscopeData(data);
        // This time step's delta rotation to be multiplied by the current rotation
        // after computing it from the gyro sample data.
        /*if (timestamp != 0) {
            final float dT = (event.timestamp - timestamp) * NS2S;
            // Axis of the rotation sample, not normalized yet.
            float axisX = event.values[0];
            float axisY = event.values[1];
            float axisZ = event.values[2];

            // Calculate the angular speed of the sample
            float omegaMagnitude = (float) Math.sqrt(axisX * axisX + axisY * axisY + axisZ * axisZ);

            // Normalize the rotation vector if it's big enough to get the axis
            if (omegaMagnitude > 0.1) {
                axisX /= omegaMagnitude;
                axisY /= omegaMagnitude;
                axisZ /= omegaMagnitude;
            }

            // Integrate around this axis with the angular speed by the time step
            // in order to get a delta rotation from this sample over the time step
            // We will convert this axis-angle representation of the delta rotation
            // into a quaternion before turning it into the rotation matrix.
            float thetaOverTwo = omegaMagnitude * dT / 2.0f;
            float sinThetaOverTwo = (float) Math.sin(thetaOverTwo);
            float cosThetaOverTwo = (float) Math.cos(thetaOverTwo);
            deltaRotationVector[0] = sinThetaOverTwo * axisX;
            deltaRotationVector[1] = sinThetaOverTwo * axisY;
            deltaRotationVector[2] = sinThetaOverTwo * axisZ;
            deltaRotationVector[3] = cosThetaOverTwo;
        }
        timestamp = event.timestamp;
        float[] deltaRotationMatrix = new float[9];
        SensorManager.getRotationMatrixFromVector(deltaRotationMatrix, deltaRotationVector);

        reportGyroscopeValues(deltaRotationMatrix);
        // User code should concatenate the delta rotation we computed with the current rotation*/
        // in order to get the updated rotation.
        // rotationCurrent = rotationCurrent * deltaRotationMatrix;
    }

    private double x, y, z = 0;
    private void reportGyroscopeValues(float[] values) {
        double lastGyroscopeX = values[0];
        double lastGyroscopeY = values[1];
        double lastGyroscopeZ = values[2];

        x = (lastGyroscopeX - x);
        y = (lastGyroscopeY - y);
        z = (lastGyroscopeZ - z);

        double[] merge = new double[6];
        merge[0] = x;
        merge[1] = y;
        merge[2] = z;

        // add raw data
        merge[3] = lastGyroscopeX;
        merge[4] = lastGyroscopeY;
        merge[5] = lastGyroscopeZ;
        //sensorCallback.onGyroscopeData(merge);
    }

}
