package au.carrsq.sensorplatform.Database;

import android.accounts.AccountManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;

import static android.content.Context.ACCOUNT_SERVICE;


/**
 * Created by julia on 20.04.2017.
 */

public class WifiWatcher extends BroadcastReceiver {

    public static final String AUTHORITY = "au.carrsq.sensorplatform.Database.SqliteDataProvider.provider";
    private static final String TAG = "WifiWatcher";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "Wifi change received");
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        if (wifiManager.isWifiEnabled() || intent.getAction().equals("android.intent.action.AIRPLANE_MODE")) {
            AccountManager accountManager = (AccountManager) context.getSystemService(ACCOUNT_SERVICE);

            Bundle extras = new Bundle();
            extras.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
            extras.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);

            ContentResolver.requestSync(accountManager.getAccounts()[0], AUTHORITY, extras);
        }
    }
}
