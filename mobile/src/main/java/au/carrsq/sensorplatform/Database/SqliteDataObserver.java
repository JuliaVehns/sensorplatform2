package au.carrsq.sensorplatform.Database;

import android.content.Context;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;

/**
 * Created by julia on 24.04.2017.
 */

public class SqliteDataObserver extends ContentObserver {
    public static final String ACTION_DATA_CHANGED = "au.carrsq.sensorplatform.Database.SqliteDataObserver.interaction.DATACHANGED";
    public boolean dataHasChanged;
    Context context;

    public SqliteDataObserver(Handler handler, Context conntext) {
        super(handler);
        this.context = conntext;
        setDataHasChanged(false);
    }

    @Override
    public void onChange(boolean selfChange) {
        this.onChange(selfChange, null);
    }

    @Override
    public void onChange(boolean selfChange, Uri uri) {
        Log.d("dataChanged", uri.toString());
        dataHasChanged = true;
    }

    public void setDataHasChanged(boolean dataHasChanged) {
        this.dataHasChanged = dataHasChanged;
    }


}

