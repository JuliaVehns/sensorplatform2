package au.carrsq.sensorplatform.Database;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.text.TextUtils;

import java.util.HashMap;

/**
 * Created by julia on 18.04.2017.
 */


public class SqliteDataProvider extends ContentProvider {
    static final String PROVIDER_NAME = "au.carrsq.sensorplatform.Database.SqliteDataProvider.provider";

    public static final Uri RAW_DATA_URI = Uri.parse("content://" + PROVIDER_NAME + "/raw_data");
    public static final Uri EVENT_DATA_URI = Uri.parse("content://" + PROVIDER_NAME + "/event_data");
    public static final Uri PHONE_INTERACTION_DATA_URI = Uri.parse("content://" + PROVIDER_NAME + "/phone_interaction_data");
    public static final Uri MVN_LIKELIHOOD_DATA_URI = Uri.parse("content://" + PROVIDER_NAME + "/mvn_likelihood_data");
    public static final Uri MVN_EVENT_DATA_URI = Uri.parse("content://" + PROVIDER_NAME + "/mvn_event_data");

    // common columns
    static final String _ID = "_id";
    private static String CODE_ID = "CODE_ID";
    private static String TIMESTAMP = "TIMESTAMP";
    private static String DATETIME = "DATETIME";
    private static String SYNC = "SYNC";

    private static String RAW_DATA_TABLE = "raw_data";
    // columns of raw_data
    private static String ACCELERATIONX = "ACCELERATIONX";
    private static String ACCELERATIONY = "ACCELERATIONY";
    private static String ACCELERATIONZ = "ACCELERATIONZ";

    private static String RAWACCELERATIONX = "RAWACCELERATIONX";
    private static String RAWACCELERATIONY = "RAWACCELERATIONY";
    private static String RAWACCELERATIONZ = "RAWACCELERATIONZ";

    private static String GRAVITYX = "GRAVITYX";
    private static String GRAVITYY = "GRAVITYY";
    private static String GRAVITYZ = "GRAVITYZ";

    private static String GYROSCOPEX = "GYROSCOPEX";
    private static String GYROSCOPEY = "GYROSCOPEY";
    private static String GYROSCOPEZ = "GYROSCOPEZ";

    private static String ROTATIONX = "ROTATIONX";
    private static String ROTATIONY = "ROTATIONY";
    private static String ROTATIONZ = "ROTATIONZ";

    private static String ROTATIONXRAD = "ROTATIONXRAD";
    private static String ROTATIONYRAD = "ROTATIONYRAD";
    private static String ROTATIONZRAD = "ROTATIONZRAD";

    private static String LATITUDE = "LATITUDE";
    private static String LONGITUDE = "LONGITUDE";

    private static String SPEEDCALCULATED = "SPEEDCALCULATED";
    private static String SPEEDLOCATIONMANAGER = "SPEEDLOCATIONMANAGER";

    // MVN-Algo
    private static String JERK = "JERK";
    private static String STEER = "STEER";
    private static String GACC = "GACC";
    private static String GBEA = "GBEA";

    private static String MAGNETICFIELDX = "MAGNETICFIELDX";
    private static String MAGNETICFIELDY = "MAGNETICFIELDY";
    private static String MAGNETICFIELDZ = "MAGNETICFIELDZ";


    private static String EVENT_DATA_TABLE = "event_data";
    // columns of event_data
    private static String LEVEL = "LEVEL";
    private static String DESCRIPTION = "DESCRIPTION";
    private static String VALUE = "VALUE";
    private static String EXTRA = "EXTRA";
    private static String VIDEOFRONT = "VIDEOFRONT";
    private static String VIDEOBACK = "VIDEOBACK";


    private static String PHONE_INTERACTION_DATA_TABLE = "phone_interaction_data";
    // columns of phone_interaction
    private static String INTERACION = "INTERACTION";


    private static String MVN_LIKELIHOOD_DATA_TABLE = "mvn_likelihood_data";
    // columns of likelihood_data
    private static String LOGINDEX = "LOGINDEX";
    private static String ALTERNATIVE = "ALTERNATIVE";
    private static String LIKELIHOOD = "LIKELIHOOD";

    private static String MVN_EVENT_DATA_TABLE = "mvn_event_data";
    // columns of mvn_event_data
    private static String EVENT = "EVENT";
    private static String SEVERITY = "SEVERITY";

    private static HashMap<String, String> RAW_DATA_MAP;
    private static HashMap<String, String> EVENT_DATA_MAP;
    private static HashMap<String, String> PHONE_INTERACTION_DATA_MAP;
    private static HashMap<String, String> MVN_LIKELIHOOD_DATA_MAP;
    private static HashMap<String, String> MVN_EVENT_DATA_MAP;

    static final int RAW_DATA = 1;
    static final int RAW_DATA_ID = 2;
    static final int EVENT_DATA = 3;
    static final int EVENT_DATA_ID = 4;
    static final int PHONE_INTERACTION_DATA = 5;
    static final int PHONE_INTERACTION_DATA_ID = 6;
    static final int MVN_LIKELIHOOD_DATA = 7;
    static final int MVN_LIKELIHOOD_DATA_ID = 8;
    static final int MVN_EVENT_DATA = 9;
    static final int MVN_EVENT_DATA_ID = 10;

    SqliteDataObserver sqliteDataObserver;

    static final UriMatcher uriMatcher;

    private boolean dbIsSetUp;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, RAW_DATA_TABLE, RAW_DATA);
        uriMatcher.addURI(PROVIDER_NAME, RAW_DATA_TABLE + "/#", RAW_DATA_ID);

        uriMatcher.addURI(PROVIDER_NAME, EVENT_DATA_TABLE, EVENT_DATA);
        uriMatcher.addURI(PROVIDER_NAME, EVENT_DATA_TABLE + "/#", EVENT_DATA_ID);

        uriMatcher.addURI(PROVIDER_NAME, PHONE_INTERACTION_DATA_TABLE, PHONE_INTERACTION_DATA);
        uriMatcher.addURI(PROVIDER_NAME, PHONE_INTERACTION_DATA_TABLE + "/#", PHONE_INTERACTION_DATA_ID);

        uriMatcher.addURI(PROVIDER_NAME, MVN_LIKELIHOOD_DATA_TABLE, MVN_LIKELIHOOD_DATA);
        uriMatcher.addURI(PROVIDER_NAME, MVN_LIKELIHOOD_DATA_TABLE + "/#", MVN_LIKELIHOOD_DATA_ID);

        uriMatcher.addURI(PROVIDER_NAME, MVN_EVENT_DATA_TABLE, MVN_EVENT_DATA);
        uriMatcher.addURI(PROVIDER_NAME, MVN_EVENT_DATA_TABLE + "/#", MVN_EVENT_DATA_ID);
    }

    public static String DB_NAME =
            Environment.getExternalStorageDirectory().getPath() + "/SensorPlatform.db";
    private SQLiteDatabase db;
    static final int DATABASE_VERSION = 17;

    static final String CREATE_RAW_DATA_TABLE =
            "CREATE TABLE IF NOT EXISTS "
                    + RAW_DATA_TABLE + "(" + CODE_ID + " TEXT, " + TIMESTAMP + " TEXT, " + DATETIME + " TEXT, " + ACCELERATIONX + " INTEGER, " + ACCELERATIONY + " INTEGER, " + ACCELERATIONZ + " INTEGER, " + RAWACCELERATIONX + " INTEGER, " + RAWACCELERATIONY + " INTEGER, " + RAWACCELERATIONZ + " INTEGER, " + GRAVITYX + " INTEGER, " + GRAVITYY + " INTEGER, " + GRAVITYZ + " INTEGER, " + GYROSCOPEX + " INTEGER, " + GYROSCOPEY + " INTEGER, " + GYROSCOPEZ + " INTEGER, " + ROTATIONX + " INTEGER, " + ROTATIONY + " INTEGER, " + ROTATIONZ + " INTEGER, " + ROTATIONXRAD + " INTEGER, " + ROTATIONYRAD + " INTEGER, " + ROTATIONZRAD + " INTEGER, " + LATITUDE + " INTEGER, " + LONGITUDE + " INTEGER, " + SPEEDCALCULATED + " INTEGER, " + SPEEDLOCATIONMANAGER + " INTEGER, " + JERK + " INTEGER, " + STEER + " INTEGER, " + GACC + " INTEGER, " + GBEA + " INTEGER, " + MAGNETICFIELDX + " INTEGER, " + MAGNETICFIELDY + " INTEGER, " + MAGNETICFIELDZ + " INTEGER, " + SYNC + " INTEGER); ";

    static final String CREATE_EVENT_DATA_TABLE = "CREATE TABLE IF NOT EXISTS "
            + EVENT_DATA_TABLE + "(" + CODE_ID + " TEXT, " + TIMESTAMP + " TEXT, " + DATETIME + " TEXT, " + LEVEL + " VARCHAR, " + DESCRIPTION + " VARCHAR, " + VALUE + " Integer, " + EXTRA + " INTEGER, " + VIDEOFRONT + " VARCHAR, " + VIDEOBACK + " VARCHAR, " + SYNC + " INTEGER); ";


    static final String CREATE_PHONE_INTERACION_DATA_TABLE = "CREATE TABLE IF NOT EXISTS "
            + PHONE_INTERACTION_DATA_TABLE + "(" + CODE_ID + " TEXT, " + TIMESTAMP + " TEXT, " + DATETIME + " TEXT, " + INTERACION + " TEXT, " + SYNC + " INTEGER); ";

    static final String CREATE_MVN_LIKELIHOOD_DATA_TABLE = "CREATE TABLE IF NOT EXISTS "
            + MVN_LIKELIHOOD_DATA_TABLE + "(" + CODE_ID + " TEXT, " + TIMESTAMP + " TEXT, " + DATETIME + " TEXT, " + LOGINDEX + " INTEGER, " + ALTERNATIVE + " INTEGER, " + LIKELIHOOD + " INTEGER, " + SYNC + " INTEGER); ";

    static final String CREATE_MVN_EVENT_DATA_TABLE = "CREATE TABLE IF NOT EXISTS "
            + MVN_EVENT_DATA_TABLE + "(" + CODE_ID + " TEXT, " + TIMESTAMP + " TEXT, " + DATETIME + " TEXT, " + EVENT + " TEXT, " + SEVERITY + " INTEGER, " + SYNC + " INTEGER); ";


    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, DB_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_RAW_DATA_TABLE);
            db.execSQL(CREATE_EVENT_DATA_TABLE);
            db.execSQL(CREATE_PHONE_INTERACION_DATA_TABLE);
            db.execSQL(CREATE_MVN_LIKELIHOOD_DATA_TABLE);
            db.execSQL(CREATE_MVN_EVENT_DATA_TABLE);

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + RAW_DATA_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + EVENT_DATA_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + PHONE_INTERACTION_DATA_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + MVN_LIKELIHOOD_DATA_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + MVN_EVENT_DATA_TABLE);
            onCreate(db);
        }
    }

    @Override
    public boolean onCreate() {
        Context context = getContext();
        dbIsSetUp = false;
        return true;

    }

    public boolean setUpDatabase(Context context) {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        sqliteDataObserver = new SqliteDataObserver(new Handler(), context);

        db = dbHelper.getWritableDatabase();
        return (db == null) ? false : true;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        if (!dbIsSetUp) {
            boolean setUp = setUpDatabase(getContext());
            dbIsSetUp = setUp;
        }

        Uri _uri = null;
        long rowID;
        switch (uriMatcher.match(uri)) {
            case RAW_DATA:
                rowID = db.insert(RAW_DATA_TABLE, "", values);
                if (rowID > 0) {
                    _uri = ContentUris.withAppendedId(RAW_DATA_URI, rowID);
                    getContext().getContentResolver().notifyChange(_uri, sqliteDataObserver);
                    return _uri;
                }
                break;
            case EVENT_DATA:
                rowID = db.insert(EVENT_DATA_TABLE, "", values);
                if (rowID > 0) {
                    _uri = ContentUris.withAppendedId(EVENT_DATA_URI, rowID);
                    getContext().getContentResolver().notifyChange(_uri, sqliteDataObserver);
                    return _uri;
                }
                break;
            case PHONE_INTERACTION_DATA:
                rowID = db.insert(PHONE_INTERACTION_DATA_TABLE, "", values);
                if (rowID > 0) {
                    _uri = ContentUris.withAppendedId(PHONE_INTERACTION_DATA_URI, rowID);
                    getContext().getContentResolver().notifyChange(_uri, sqliteDataObserver);
                    return _uri;
                }
                break;
            case MVN_LIKELIHOOD_DATA:
                rowID = db.insert(MVN_LIKELIHOOD_DATA_TABLE, "", values);
                if (rowID > 0) {
                    _uri = ContentUris.withAppendedId(MVN_LIKELIHOOD_DATA_URI, rowID);
                    getContext().getContentResolver().notifyChange(_uri, sqliteDataObserver);
                    return _uri;
                }
                break;
            case MVN_EVENT_DATA:
                rowID = db.insert(MVN_EVENT_DATA_TABLE, "", values);
                if (rowID > 0) {
                    _uri = ContentUris.withAppendedId(MVN_EVENT_DATA_URI, rowID);
                    getContext().getContentResolver().notifyChange(_uri, sqliteDataObserver);
                    return _uri;
                }
                break;
            default:
                throw new SQLException("Failed to add a record into " + uri);
        }

        return _uri;
    }

    @Override
    public Cursor query(Uri uri, String[] projection,
                        String selection, String[] selectionArgs, String sortOrder) {

        if (!dbIsSetUp) {
            boolean setUp = setUpDatabase(getContext());
            dbIsSetUp = setUp;
        }

        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        switch (uriMatcher.match(uri)) {
            case RAW_DATA:
                qb.setTables(RAW_DATA_TABLE);
                qb.setProjectionMap(RAW_DATA_MAP);
                break;
            case RAW_DATA_ID:
                qb.setTables(RAW_DATA_TABLE);
                qb.appendWhere(_ID + "=" + uri.getPathSegments().get(1));
                break;

            case EVENT_DATA:
                qb.setTables(EVENT_DATA_TABLE);
                qb.setProjectionMap(EVENT_DATA_MAP);
                break;
            case EVENT_DATA_ID:
                qb.setTables(EVENT_DATA_TABLE);
                qb.appendWhere(_ID + "=" + uri.getPathSegments().get(1));
                break;

            case PHONE_INTERACTION_DATA:
                qb.setTables(PHONE_INTERACTION_DATA_TABLE);
                qb.setProjectionMap(PHONE_INTERACTION_DATA_MAP);
                break;
            case PHONE_INTERACTION_DATA_ID:
                qb.setTables(PHONE_INTERACTION_DATA_TABLE);
                qb.appendWhere(_ID + "=" + uri.getPathSegments().get(1));
                break;

            case MVN_LIKELIHOOD_DATA:
                qb.setTables(MVN_LIKELIHOOD_DATA_TABLE);
                qb.setProjectionMap(MVN_LIKELIHOOD_DATA_MAP);
                break;
            case MVN_LIKELIHOOD_DATA_ID:
                qb.setTables(MVN_LIKELIHOOD_DATA_TABLE);
                qb.appendWhere(_ID + "=" + uri.getPathSegments().get(1));
                break;

            case MVN_EVENT_DATA:
                qb.setTables(MVN_EVENT_DATA_TABLE);
                qb.setProjectionMap(MVN_EVENT_DATA_MAP);
                break;
            case MVN_EVENT_DATA_ID:
                qb.setTables(MVN_EVENT_DATA_TABLE);
                qb.appendWhere(_ID + "=" + uri.getPathSegments().get(1));
                break;

            default:
        }

        if (sortOrder == null || sortOrder == "") {
            sortOrder = CODE_ID;
        }

        Cursor c = qb.query(db, projection, selection,
                selectionArgs, null, null, sortOrder);
        c.setNotificationUri(getContext().getContentResolver(), uri);

        return c;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        if (!dbIsSetUp) {
            boolean setUp = setUpDatabase(getContext());
            dbIsSetUp = setUp;
        }
        int count = 0;
        String id;
        switch (uriMatcher.match(uri)) {
            case RAW_DATA:
                count = db.delete(RAW_DATA_TABLE, selection, selectionArgs);
                break;
            case RAW_DATA_ID:
                id = uri.getPathSegments().get(1);
                count = db.delete(RAW_DATA_TABLE, _ID + " = " + id +
                        (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : ""), selectionArgs);
                break;

            case EVENT_DATA:
                count = db.delete(EVENT_DATA_TABLE, selection, selectionArgs);
                break;
            case EVENT_DATA_ID:
                id = uri.getPathSegments().get(1);
                count = db.delete(EVENT_DATA_TABLE, _ID + " = " + id +
                        (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : ""), selectionArgs);
                break;

            case PHONE_INTERACTION_DATA:
                count = db.delete(PHONE_INTERACTION_DATA_TABLE, selection, selectionArgs);
                break;
            case PHONE_INTERACTION_DATA_ID:
                id = uri.getPathSegments().get(1);
                count = db.delete(PHONE_INTERACTION_DATA_TABLE, _ID + " = " + id +
                        (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : ""), selectionArgs);
                break;

            case MVN_LIKELIHOOD_DATA:
                count = db.delete(MVN_LIKELIHOOD_DATA_TABLE, selection, selectionArgs);
                break;
            case MVN_LIKELIHOOD_DATA_ID:
                id = uri.getPathSegments().get(1);
                count = db.delete(MVN_LIKELIHOOD_DATA_TABLE, _ID + " = " + id +
                        (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : ""), selectionArgs);
                break;

            case MVN_EVENT_DATA:
                count = db.delete(MVN_EVENT_DATA_TABLE, selection, selectionArgs);
                break;
            case MVN_EVENT_DATA_ID:
                id = uri.getPathSegments().get(1);
                count = db.delete(MVN_EVENT_DATA_TABLE, _ID + " = " + id +
                        (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : ""), selectionArgs);
                break;

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, sqliteDataObserver);
        return count;
    }

    @Override
    public int update(Uri uri, ContentValues values,
                      String selection, String[] selectionArgs) {

        if (!dbIsSetUp) {
            boolean setUp = setUpDatabase(getContext());
            dbIsSetUp = setUp;
        }
        int count = 0;
        switch (uriMatcher.match(uri)) {
            case RAW_DATA:
                count = db.update(RAW_DATA_TABLE, values, selection, selectionArgs);
                break;

            case RAW_DATA_ID:
                count = db.update(RAW_DATA_TABLE, values,
                        _ID + " = " + uri.getPathSegments().get(1) +
                                (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : ""), selectionArgs);
                break;

            case EVENT_DATA:
                count = db.update(EVENT_DATA_TABLE, values, selection, selectionArgs);
                break;
            case EVENT_DATA_ID:
                count = db.update(EVENT_DATA_TABLE, values,
                        _ID + " = " + uri.getPathSegments().get(1) +
                                (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : ""), selectionArgs);
                break;

            case PHONE_INTERACTION_DATA:
                count = db.update(PHONE_INTERACTION_DATA_TABLE, values, selection, selectionArgs);
                break;
            case PHONE_INTERACTION_DATA_ID:
                count = db.update(PHONE_INTERACTION_DATA_TABLE, values,
                        _ID + " = " + uri.getPathSegments().get(1) +
                                (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : ""), selectionArgs);
                break;

            case MVN_LIKELIHOOD_DATA:
                count = db.update(MVN_LIKELIHOOD_DATA_TABLE, values, selection, selectionArgs);
                break;
            case MVN_LIKELIHOOD_DATA_ID:
                count = db.update(MVN_LIKELIHOOD_DATA_TABLE, values,
                        _ID + " = " + uri.getPathSegments().get(1) +
                                (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : ""), selectionArgs);
                break;

            case MVN_EVENT_DATA:
                count = db.update(MVN_EVENT_DATA_TABLE, values, selection, selectionArgs);
                break;
            case MVN_EVENT_DATA_ID:
                count = db.update(MVN_EVENT_DATA_TABLE, values,
                        _ID + " = " + uri.getPathSegments().get(1) +
                                (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : ""), selectionArgs);
                break;

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, sqliteDataObserver);
        return count;
    }

    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)) {
            case RAW_DATA:
                return "vnd.android.cursor.dir/vnd.example.raw_data";
            case RAW_DATA_ID:
                return "vnd.android.cursor.item/vnd.example.raw_dat";
            case EVENT_DATA:
                return "vnd.android.cursor.dir/vnd.example.event_data";
            case EVENT_DATA_ID:
                return "vnd.android.cursor.item/vnd.example.event_dat";
            case PHONE_INTERACTION_DATA:
                return "vnd.android.cursor.dir/vnd.example.phone_interaction_data";
            case PHONE_INTERACTION_DATA_ID:
                return "vnd.android.cursor.item/vnd.example.phone_interaction_dat";
            case MVN_LIKELIHOOD_DATA:
                return "vnd.android.cursor.dir/vnd.example.mvn_likelihood_data";
            case MVN_LIKELIHOOD_DATA_ID:
                return "vnd.android.cursor.item/vnd.example.mvn_likelihood_dat";
            case MVN_EVENT_DATA:
                return "vnd.android.cursor.dir/vnd.example.mvn_event_data";
            case MVN_EVENT_DATA_ID:
                return "vnd.android.cursor.item/vnd.example.mvn-event_dat";
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }


    public static class SqlLiteHelper extends SQLiteOpenHelper {

        public static String DB_NAME =
                Environment.getExternalStorageDirectory().getPath() + "/SensorPlatform.db";

        public SqlLiteHelper(Context context) {
            super(context, DB_NAME, null, 1);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }


    }


}

