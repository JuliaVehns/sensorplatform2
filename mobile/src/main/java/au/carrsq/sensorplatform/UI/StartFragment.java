package au.carrsq.sensorplatform.UI;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import au.carrsq.sensorplatform.Core.MainActivity;
import au.carrsq.sensorplatform.R;
import au.carrsq.sensorplatform.Utilities.PhoneInteractionService;

public class StartFragment extends Fragment {
    private FrameLayout startStudy;

    private TextInputLayout input_cid;
    private String c_id;
    private TextInputEditText code_id;

    View.OnClickListener startStudyButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            boolean valid = validateInputs();
            if (valid) {
                writePreferences();

              /*  // start logging interactions of user
                Intent phoneInteractionIntent = new Intent(getContext().getApplicationContext(), PhoneInteractionService.class);
                phoneInteractionIntent.addCategory(PhoneInteractionService.TAG);
                getActivity().startService(phoneInteractionIntent);*/

                MainActivity app = (MainActivity) getActivity();
                startStudy();
                app.goToAppFragment();
            }
        }
    };

    public StartFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_start_2, container, false);

        startStudy = (FrameLayout) v.findViewById(R.id.startStudyButton);

        startStudy.setOnClickListener(startStudyButtonListener);

        code_id = (TextInputEditText) v.findViewById(R.id.code_id);
        input_cid = (TextInputLayout) v.findViewById(R.id.input_layout_code_id);



        SharedPreferences studyPrefs = getActivity().getSharedPreferences(getString(R.string.study_preferences_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = studyPrefs.edit();
        editor.putString("code_id", c_id);
        editor.commit();



        MainActivity app = (MainActivity) getActivity();
        if (app.started) {
            Log.d("START_FRAGMENT", "serviceRunning = true");
        }

        return v;
    }

    private boolean validateInputs() {
        c_id = code_id.getText().toString();
        if (c_id.trim().isEmpty()) {
            input_cid.setError("Please fill in your code.");
            return false;
        } else {
            input_cid.setErrorEnabled(false);
        }
        return true;
    }

    private void startStudy() {
        ((MainActivity) getActivity()).startMeasuring();

    }

    private void writePreferences() {
        SharedPreferences studyPrefs = getActivity().getSharedPreferences(getString(R.string.study_preferences_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = studyPrefs.edit();
        editor.putString("code_id", c_id);
        editor.commit();
    }
}
