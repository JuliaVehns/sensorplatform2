package au.carrsq.sensorplatform.UI;


import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.text.DecimalFormat;

import au.carrsq.sensorplatform.Core.ActiveSubscriptions;
import au.carrsq.sensorplatform.Core.MainActivity;
import au.carrsq.sensorplatform.Core.Preferences;
import au.carrsq.sensorplatform.Core.SensorPlatformService;
import au.carrsq.sensorplatform.Database.SqliteDataProvider;
import au.carrsq.sensorplatform.MVNAlgorithm.RunMVNClassifier;
import au.carrsq.sensorplatform.Models.EventVector;
import au.carrsq.sensorplatform.Models.RawDataVector;
import au.carrsq.sensorplatform.R;
import au.carrsq.sensorplatform.Utilities.PhoneInteractionService;

/**
 * This fragment shows the real-time raw and event data
 */
public class AppFragment extends Fragment {
    public SqliteDataProvider.SqlLiteHelper dbHelper;

    SharedPreferences sensor_prefs;
    SharedPreferences setting_prefs;

    RelativeLayout dataLayout;
    TextView waitingText;
    TextView collectingText;

    LinearLayout accelerometerInfo;
    TextView accX;
    TextView accY;
    TextView accZ;

    LinearLayout rotationInfo;
    TextView rotX;
    TextView rotY;
    TextView rotZ;

    TextView lat;
    TextView lon;
    TextView speed;

    TextView light;

    TextView street;
    TextView event;
    TextView face;
    TextView car;

    Button stopButton;
    Button pauseButton;
    Button resumeButton;
    Button showData;

    DecimalFormat df = new DecimalFormat("#.####");

    boolean rawRegistered = false;
    boolean eventRegistered = false;

    public AppFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        sensor_prefs = getActivity().getSharedPreferences(getActivity().getString(R.string.sensor_preferences_key), Context.MODE_PRIVATE);
        setting_prefs = getActivity().getSharedPreferences(getActivity().getString(R.string.settings_preferences_key), Context.MODE_PRIVATE);

        //open sqlite db
        dbHelper = new SqliteDataProvider.SqlLiteHelper(this.getContext());

    }

    private void registerReceivers() {
        IntentFilter f = new IntentFilter("au.carrsq.sensorplatform.RawData");
        rawRegistered = true;
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(rawReceiver, f);

        IntentFilter f2 = new IntentFilter("au.carrsq.sensorplatform.EventData");
        eventRegistered = true;
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(eventReceiver, f2);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container != null) {
            container.removeAllViews();
        }

        View v = inflater.inflate(R.layout.fragment_app, container, false);

        dataLayout = (RelativeLayout) v.findViewById(R.id.dataLayout);
        waitingText = (TextView) v.findViewById(R.id.waiting_text);
        collectingText = (TextView) v.findViewById(R.id.collectingText);

        accelerometerInfo = (LinearLayout) v.findViewById(R.id.accelerometerInfo);
        accX = (TextView) v.findViewById(R.id.accXText);
        accY = (TextView) v.findViewById(R.id.accYText);
        accZ = (TextView) v.findViewById(R.id.accZText);

        rotationInfo = (LinearLayout) v.findViewById(R.id.rotationInfo);
        rotX = (TextView) v.findViewById(R.id.rotXText);
        rotY = (TextView) v.findViewById(R.id.rotYText);
        rotZ = (TextView) v.findViewById(R.id.rotZText);

        lat = (TextView) v.findViewById(R.id.latText);
        lon = (TextView) v.findViewById(R.id.lonText);
        speed = (TextView) v.findViewById(R.id.speedText);
        light = (TextView) v.findViewById(R.id.lightText);

        street = (TextView) v.findViewById(R.id.osmText);
        event = (TextView) v.findViewById(R.id.eventText);
        face = (TextView) v.findViewById(R.id.faceText);
        car = (TextView) v.findViewById(R.id.carText);

        stopButton = (Button) v.findViewById(R.id.stopButton);
        pauseButton = (Button) v.findViewById(R.id.pauseButton);
        resumeButton = (Button) v.findViewById(R.id.resumeButton);
        showData = (Button) v.findViewById(R.id.toggleData);

        stopButton.setOnClickListener(stopListener);
        pauseButton.setOnClickListener(pauseListener);
        resumeButton.setOnClickListener(resumeListener);

        showData.setOnClickListener(toggleDataListener);

        return v;
    }

    View.OnClickListener stopListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent stopIntent = new Intent(getActivity(), SensorPlatformService.class);
            stopIntent.setAction("SERVICE_STOP");

            Intent phoneInteractionIntent = new Intent(getContext().getApplicationContext(), PhoneInteractionService.class);
            phoneInteractionIntent.addCategory(PhoneInteractionService.TAG);
            getActivity().stopService(phoneInteractionIntent);

            MainActivity app = (MainActivity) getActivity();
            try {
                app.unbindService(app.getConnection());
            } catch (Exception e) {
                e.printStackTrace();
            }
            getActivity().startService(stopIntent);

            resumeButton.setEnabled(false);
            pauseButton.setEnabled(false);


            MainActivity.started = false;
            MainActivity.mBound = false;

            app.goToStartFragment(25, false);
        }
    };

    View.OnClickListener pauseListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent pauseIntent = new Intent(getActivity(), SensorPlatformService.class);
            pauseIntent.setAction("SERVICE_PAUSE");

            getActivity().startService(pauseIntent);
            resumeButton.setEnabled(true);
            pauseButton.setEnabled(false);
        }
    };

    View.OnClickListener resumeListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent resumeIntent = new Intent(getActivity(), SensorPlatformService.class);
            resumeIntent.setAction("SERVICE_RESUME");

            getActivity().startService(resumeIntent);
            resumeButton.setEnabled(false);
            pauseButton.setEnabled(true);
        }
    };

    View.OnClickListener toggleDataListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (dataLayout.getVisibility() == View.INVISIBLE) {
                waitingText.setVisibility(View.INVISIBLE);
                collectingText.setVisibility(View.INVISIBLE);
                dataLayout.setVisibility(View.VISIBLE);
                showData.setText("Hide Data");
            } else {
                waitingText.setVisibility(View.VISIBLE);
                SharedPreferences studyPrefs = getActivity().getSharedPreferences(getString(R.string.study_preferences_key), Context.MODE_PRIVATE);
                waitingText.append(studyPrefs.getString("code_id", ""));
                collectingText.setVisibility(View.INVISIBLE);
                dataLayout.setVisibility(View.INVISIBLE);
                showData.setText("Show Data");
            }
        }
    };

    public void updateUI(RawDataVector vector) {
        final RawDataVector v = vector;

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (Preferences.accelerometerActivated(sensor_prefs)) {
                    accelerometerInfo.setVisibility(View.VISIBLE);
                    accX.setText("AccX: " + df.format(v.getAccelerationX()));
                    accY.setText("AccY: " + df.format(v.getAccelerationY()));
                    accZ.setText("AccZ: " + df.format(v.getAccelerationZ()));
                }

                if (Preferences.rotationActivated(sensor_prefs)) {
                    rotationInfo.setVisibility(View.VISIBLE);
                    rotX.setText("RotX: " + df.format(v.getRotationXRad()));
                    rotY.setText("RotY: " + df.format(v.getRotationYRad()));
                    rotZ.setText("RotZ: " + df.format(v.getRotationZRad()));
                }

                if (v.latitude == 0 && ActiveSubscriptions.usingGPS()) {
                    lat.setText("Lat: Acquiring signal…");
                    lon.setText("Lon: Acquiring signal…");
                    speed.setText("Speed: Acquiring signal…");
                }

                if (v.latitude != 0 && ActiveSubscriptions.usingGPS()) {
                    lat.setText("Lat: " + df.format(v.latitude));
                    lon.setText("Lon: " + df.format(v.longitude));
                    speed.setText("Speed: " + df.format(v.speedLocationManager) + " km/h");
                }
            }
        });

    }

    public void updateUI(EventVector vector) {
        final EventVector v = vector;

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (v.getEventDescription().contains("ROAD"))
                    street.setText(v.getEventDescription());
                else if (v.getEventDescription().equals("Face detected"))
                    face.setText("Face detected: YES");
                else if (v.getEventDescription().equals("No Face detected"))
                    face.setText("Face detected: NO");
                else if (v.getEventDescription().contains("Cars"))
                    car.setText(v.getEventDescription() + ": " + v.getValue());
                else
                    event.setText(v.getLevelString() + ": " + v.getEventDescription() + ", " + df.format(v.getValue()));
            }
        });
    }

    BroadcastReceiver rawReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle b = intent.getExtras();
            RawDataVector v = new Gson().fromJson(b.getString("RawData"), RawDataVector.class);

            // write raw data to the sqllite database
            addRawDataToRawDataProvider(v);

            //Log.d("RawData @ App  ", v.toString());

            if (waitingText.getVisibility() == View.VISIBLE) {
                waitingText.setVisibility(View.INVISIBLE);
                collectingText.setVisibility((View.VISIBLE));
            }

            if (getActivity() != null)
                updateUI(v);
        }
    };

    BroadcastReceiver eventReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle b = intent.getExtras();
            EventVector v = new Gson().fromJson(b.getString("EventData"), EventVector.class);

            // write event data to the sqllite database
            addEventDataToRawDataProvider(v);
            Log.d("EventData @ App  ", v.toString());

            if (v.getEventDescription().equals("Trip End detected"))
                handleTripEndUI();

            if (getActivity() != null)
                updateUI(v);
        }
    };

    private int rawDataCounter = 0;

    private void addRawDataToRawDataProvider(RawDataVector v) {

        ContentValues contentValues = new ContentValues();

        contentValues.put("code_id", v.getCode_id());
        contentValues.put("timestamp", String.valueOf(v.getTimestamp()));
        contentValues.put("datetime", v.getDateTime());

        contentValues.put("accelerationX", v.getAccelerationX());
        contentValues.put("accelerationY", v.getAccelerationY());
        contentValues.put("accelerationZ", v.getAccelerationZ());

        contentValues.put("rawAccelerationX", v.getRawAccelerationX());
        contentValues.put("rawAccelerationY", v.getRawAccelerationY());
        contentValues.put("rawAccelerationZ", v.getRawAccelerationZ());

        contentValues.put("gravityX", v.getGravityX());
        contentValues.put("gravityY", v.getGravityY());
        contentValues.put("gravityZ", v.getGravityZ());

        contentValues.put("gyroscopeX", v.getGyroscopeX());
        contentValues.put("gyroscopeY", v.getGyroscopeY());
        contentValues.put("gyroscopeZ", v.getGyroscopeZ());

        contentValues.put("rotationX", v.getRotationX());
        contentValues.put("rotationY", v.getRotationY());
        contentValues.put("rotationZ", v.getRotationZ());

        contentValues.put("rotationXRad", v.getRotationXRad());
        contentValues.put("rotationYRad", v.getRotationYRad());
        contentValues.put("rotationZRad", v.getRotationZRad()); // same as gbea --> time derivative of the GPS azimuth

        contentValues.put("latitude", v.getLatitude());
        contentValues.put("longitude", v.getLongitude());

        contentValues.put("speedCalculated", v.getSpeedCalculated());
        contentValues.put("speedLocationManager", v.getSpeedLocationManager());

        contentValues.put("magneticFieldX", v.getMagneticFieldX());
        contentValues.put("magneticFieldY", v.getMagneticFieldY());
        contentValues.put("magneticFieldZ", v.getMagneticFieldZ());

        contentValues.put("jerk", v.getJerk());
        contentValues.put("steer", v.getSteer());
        contentValues.put("gacc", v.getGacc());
        contentValues.put("gbea", v.getGbea());

        contentValues.put("sync", 0);

        if (this.getContext() != null) {
            // avoid making too many database querys
            if (rawDataCounter == 0) {
                rawDataCounter = this.getContext().getContentResolver().query(SqliteDataProvider.RAW_DATA_URI, null, "sync = ?", new String[]{"0"}, "").getCount();
            }
            Uri uri = this.getContext().getContentResolver().insert(
                    SqliteDataProvider.RAW_DATA_URI, contentValues);
            rawDataCounter++;

            final RawDataVector dataVector = v;
            // re-train all 1200 Samples
            if (rawDataCounter % 300 == 0) {
                new Thread(new Runnable() {
                    public void run() {
                        RunMVNClassifier runMVNClassifier = new RunMVNClassifier(getContext(), dataVector.getCode_id());
                        runMVNClassifier.main(0, 4);
                        rawDataCounter = getActivity().getContentResolver().query(SqliteDataProvider.RAW_DATA_URI, null, "sync = ?", new String[]{"0"}, "").getCount();
                    }
                }).start();


            }
        }

    }


    private void addEventDataToRawDataProvider(EventVector v) {
        ContentValues contentValues = new ContentValues();
        SharedPreferences study_prefs = this.getContext().getSharedPreferences(this.getString(R.string.study_preferences_key), Context.MODE_PRIVATE);

        contentValues.put("code_id", study_prefs.getString("code_id", ""));
        contentValues.put("timestamp", v.getTimestamp());
        contentValues.put("datetime", v.getDateTime());
        contentValues.put("level", v.getLevelString());
        contentValues.put("description", v.getEventDescription());
        contentValues.put("value", v.getValue());
        contentValues.put("extra", v.getExtraValue());
        contentValues.put("videofront", v.getVideoFront());
        contentValues.put("videoback", v.getVideoBack());
        contentValues.put("sync", 0);

        if (this.getContext() != null) {
            Uri uri = this.getContext().getContentResolver().insert(
                    SqliteDataProvider.EVENT_DATA_URI, contentValues);
        }
    }

    private void handleTripEndUI() {
        // wait for last raw data to arrive before changing UI
        Handler h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                waitingText.setVisibility(View.VISIBLE);
                collectingText.setVisibility(View.INVISIBLE);
                dataLayout.setVisibility(View.INVISIBLE);
                showData.setText("Show Data");
            }
        }, Preferences.getRawDataDelay(setting_prefs) + 50);
    }

    @Override
    public void onPause() {
        super.onPause();

        if (rawRegistered) {
            try {
                LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(rawReceiver);
                rawRegistered = false;
            } catch (IllegalArgumentException e) {
                Log.e("APP FRAGMENT", e.toString());
            }

        }

        if (eventRegistered) {
            try {
                LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(eventReceiver);
                eventRegistered = false;
            } catch (IllegalArgumentException e) {
                Log.e("APP FRAGMENT", e.toString());
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!rawRegistered || !eventRegistered)
            registerReceivers();
    }
}
