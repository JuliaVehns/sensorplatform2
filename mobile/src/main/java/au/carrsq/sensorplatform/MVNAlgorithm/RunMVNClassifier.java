package au.carrsq.sensorplatform.MVNAlgorithm;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.threeten.bp.Instant;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.ZoneId;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import au.carrsq.sensorplatform.Database.SqliteDataProvider;
import au.carrsq.sensorplatform.Models.MVNEventData;
import au.carrsq.sensorplatform.Models.MVNLikelihoodData;
import au.carrsq.sensorplatform.Utilities.IO;


public class RunMVNClassifier {

    public static final String TAG = "RunMVNClassifier";
    static ContentResolver mContentResolver;
    static String evaluation_filename;
    static int num_alternatives;
    static int log_index;
    static int current_log_index;

    private FileOutputStream outLikelihood;
    private FileOutputStream output;
    public String code_id;

    private Context context;

    public RunMVNClassifier(Context con, String current_code_id) {
        mContentResolver = con.getContentResolver();
        context = con;
        code_id = current_code_id;
    }

    public static void deleteFile(String fileName) {
        try {
            File file = new File(fileName);
            file.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void main(int log_i, int num_altern) {
        evaluation_filename = "";

        log_index = log_i;
        current_log_index = log_index;
        num_alternatives = num_altern;

        try {
            outLikelihood = context.openFileOutput("likelihood-" + current_log_index + ".txt", Context.MODE_PRIVATE);
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        if (log_index == 0) {
            mContentResolver.delete(SqliteDataProvider.MVN_LIKELIHOOD_DATA_URI, "code_id = ?", new String[]{this.code_id});
            for (int i = 1; i <= 4; i++) {
                process(i);
                current_log_index = i;
            }
        } else {
            process(log_index);
        }


        if (log_index == 0) {
            evaluation_filename = "";
        }

        try {
            outLikelihood.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void process(int log_index) {
        switch (log_index) {
            case 1:
                System.out.println("- Acceleration-Braking Selected -");
                evaluation_filename = "log-ACC-BRK";
                break;
            case 2:
                System.out.println("- Lane Change 3 Selected -");
                evaluation_filename = "log-LC3";
                break;
            case 3:
                System.out.println("- Slalom Selected -");
                evaluation_filename = "log-SLALOM";
                break;
            case 4:
                System.out.println("- U-Turn Selected -");
                evaluation_filename = "log-UTURN";
                break;
            case 5:
                System.out.println("- TRAINLUX Selected -");
                evaluation_filename = "log-TRAINLUX";
                break;
            case 6:
                System.out.println("- TRAINCALM Selected -");
                evaluation_filename = "log-traincalm";
                break;

            case 7:
                System.out.println("- TRAINAGG Selected -");
                evaluation_filename = "log-trainagg";
                break;

            case 8:
                System.out.println("- BMW Selected -");
                evaluation_filename = "log-BMW";
                break;

            default:
                System.out.println("First argument should be in the range of 1-4. Exit.");
                return;
        }
        // old: "script1"
        feedData(evaluation_filename, num_alternatives);
        System.out.println("");
        System.out.println("- Program exits. -");

    }

    // @Julia feedData(log-BMW, log-ACC-BRK, 4)
    public void feedData(String evaluation_filename, int num_alternatives) {

        System.out.println("");
        // read traning file
        Cursor cursor = mContentResolver.query(SqliteDataProvider.RAW_DATA_URI, null, "sync = ?", new String[]{"0"}, "");
        int countLines = cursor.getCount();
        cursor.close();

        System.out.println("Training data contains: " + countLines + " lines.");
        if (countLines == 0) {
            System.out.println("Training data doesn't contain lines. Exit.");
            return;
        }

        // Iterates Feature Sets
        for (int i = 1; i < num_alternatives + 1; i++) {
            System.out.println("");
            MVNNewClassifier classifier = new MVNNewClassifier(i, countLines, false);

            long training_time = System.currentTimeMillis();
            System.out.println("Training MVN with Feature Set # " + i);
            System.out.println("This may take several seconds...");

            processSamples("Training", classifier, false);

            System.out.println("Training finished in " + Long.toString(System.currentTimeMillis() - training_time) + " miliseconds.");
            System.out.println("");
            System.out.println("Evaluating MVN with Feature Set # " + i);
            System.out.println("This may take several seconds...");
            long evaluating_time = System.currentTimeMillis();

            processSamples(evaluation_filename, classifier, true);

            System.out.println("Evaluation finished in " + Long.toString(System.currentTimeMillis() - evaluating_time) + " miliseconds.");
        }

    }

    public void processSamples(String filename, MVNNewClassifier classifier, boolean evaluate) {
        FileOutputStream out;
        try {

            //deleteFile(filename + "-" + classifier.getAlternativeSelected() + ".txt");
            String event = filename + "-" + classifier.getAlternativeSelected();
            int c = mContentResolver.delete(SqliteDataProvider.MVN_EVENT_DATA_URI, "code_id=? AND event=?", new String[]{this.code_id, event});

            out = context.openFileOutput(filename + "-" + classifier.getAlternativeSelected() + ".txt", Context.MODE_PRIVATE);
            String line = null;
            double likelihood = 0;

            // receive all data from database instead of the .txt file
            JSONArray jsonArrayData = getDataFromDB();
            double[] data = new double[5];

            if (jsonArrayData != null) {
                for (int i = 0; i < jsonArrayData.length(); i++) {
                    JSONObject item = jsonArrayData.getJSONObject(i);

                    // get the input parameters for the algorithm
                    data[0] = item.optDouble("jerk", 0);
                    data[1] = item.optDouble("steer", 0);
                    data[2] = item.optDouble("gacc", 0);
                    data[3] = item.optDouble("gbea", 0);
                    data[4] = item.optDouble("speedLocationManager", 0);

                    // set the input data for the algorithm
                    classifier.setInput(data);

                    if (evaluate) {
                        double severity = classifier.evaluate();
                        if (severity != 0) {
                            File output = new File(IO.baseDir + File.separator + System.currentTimeMillis() / 1000 + "_severity.txt");
                            System.out.println("severity != 0");
                        }
                        out.write(String.valueOf(item.optString("datetime", "no-datetime") + "\t" + Double.toString(severity) + "\n").getBytes());

                        MVNEventData mvnEventData = new MVNEventData();
                        mvnEventData.setCode_id(this.code_id);
                        mvnEventData.setTimestamp(System.currentTimeMillis() / 1000);
                        mvnEventData.setDateTime(LocalDateTime.ofInstant(Instant.now(), ZoneId.systemDefault()).toString());
                        mvnEventData.setEvent(filename + "-" + classifier.getAlternativeSelected());
                        mvnEventData.setSeverity(severity);

                        ContentValues contentValues = new ContentValues();
                        contentValues.put("code_id", mvnEventData.getCode_id());
                        contentValues.put("timestamp", mvnEventData.getTimestamp());
                        contentValues.put("datetime", mvnEventData.getDateTime());
                        contentValues.put("event", mvnEventData.getEvent());
                        contentValues.put("severity", mvnEventData.getSeverity());
                        contentValues.put("sync", mvnEventData.getSync());

                        mContentResolver.insert(SqliteDataProvider.MVN_EVENT_DATA_URI, contentValues);


                        double probability = classifier.probability();
                        if (probability == 0)
                            System.out.print(line);
                        // out.write(String.valueOf(item.optString("datetime", "no-datetime") + "\t" +item.optString("jerk", "no-jerk") + "\t").getBytes());
                        likelihood += Math.log(probability);
                    }
                }
            }


            if (evaluate) {
                System.out.print("Log-likelihood: " + likelihood);
                try {
                    outLikelihood.write((String.valueOf(current_log_index + "	" + classifier.getAlternativeSelected() + "	" + likelihood) + "\n").getBytes());

                    MVNLikelihoodData mvnLikelihoodData = new MVNLikelihoodData();
                    mvnLikelihoodData.setCode_id(this.code_id);
                    mvnLikelihoodData.setTimestamp(System.currentTimeMillis() / 1000);
                    mvnLikelihoodData.setDateTime(LocalDateTime.ofInstant(Instant.now(), ZoneId.systemDefault()).toString());
                    mvnLikelihoodData.setLogIndex(current_log_index);
                    mvnLikelihoodData.setAlternative(classifier.getAlternativeSelected());
                    mvnLikelihoodData.setLikelihood(likelihood);

                    ContentValues contentValues = new ContentValues();
                    contentValues.put("code_id", mvnLikelihoodData.getCode_id());
                    contentValues.put("timestamp", mvnLikelihoodData.getTimestamp());
                    contentValues.put("datetime", mvnLikelihoodData.getDateTime());
                    contentValues.put("logindex", mvnLikelihoodData.getLogIndex());
                    contentValues.put("alternative", mvnLikelihoodData.getAlternative());
                    contentValues.put("likelihood", mvnLikelihoodData.getLikelihood());
                    contentValues.put("sync", mvnLikelihoodData.getSync());

                    mContentResolver.insert(SqliteDataProvider.MVN_LIKELIHOOD_DATA_URI, contentValues);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static JSONArray getDataFromDB() {
        Log.d(TAG, "Get Data From DB");
        try {
            Cursor cursor = mContentResolver.query(SqliteDataProvider.RAW_DATA_URI, null, null, new String[]{}, null);
            if (cursor.getCount() != 0) {
                JSONObject jObj;
                JSONArray jArr = new JSONArray();
                cursor.moveToFirst();

                do {
                    jObj = new JSONObject();
                    jObj.put("code_timestamp_id", cursor.getString(cursor.getColumnIndex("CODE_ID")) + "_" + cursor.getString(cursor.getColumnIndex("DATETIME")));
                    jObj.put("code_id", cursor.getString(cursor.getColumnIndex("CODE_ID")));
                    jObj.put("timestamp", cursor.getInt(cursor.getColumnIndex("TIMESTAMP")));
                    jObj.put("datetime", cursor.getString(cursor.getColumnIndex("DATETIME")));

                    jObj.put("accelerationX", cursor.getDouble(cursor.getColumnIndex("ACCELERATIONX")));
                    jObj.put("accelerationY", cursor.getDouble(cursor.getColumnIndex("ACCELERATIONY")));
                    jObj.put("accelerationZ", cursor.getDouble(cursor.getColumnIndex("ACCELERATIONZ")));
                    jObj.put("rawAccelerationX", cursor.getDouble(cursor.getColumnIndex("RAWACCELERATIONX")));
                    jObj.put("rawAccelerationY", cursor.getDouble(cursor.getColumnIndex("RAWACCELERATIONY")));
                    jObj.put("rawAccelerationZ", cursor.getDouble(cursor.getColumnIndex("RAWACCELERATIONZ")));

                    jObj.put("gravityX", cursor.getDouble(cursor.getColumnIndex("GRAVITYX")));
                    jObj.put("gravityY", cursor.getDouble(cursor.getColumnIndex("GRAVITYY")));
                    jObj.put("gravityZ", cursor.getDouble(cursor.getColumnIndex("GRAVITYZ")));

                    jObj.put("gyroscopeX", cursor.getFloat(cursor.getColumnIndex("GYROSCOPEX")));
                    jObj.put("gyroscopeY", cursor.getFloat(cursor.getColumnIndex("GYROSCOPEY")));
                    jObj.put("gyroscopeZ", cursor.getFloat(cursor.getColumnIndex("GYROSCOPEZ")));


                    jObj.put("rotationX", cursor.getDouble(cursor.getColumnIndex("ROTATIONX")));
                    jObj.put("rotationY", cursor.getDouble(cursor.getColumnIndex("ROTATIONY")));
                    jObj.put("rotationZ", cursor.getDouble(cursor.getColumnIndex("ROTATIONZ")));
                    jObj.put("rotationXRad", cursor.getDouble(cursor.getColumnIndex("ROTATIONXRAD")));
                    jObj.put("rotationYRad", cursor.getDouble(cursor.getColumnIndex("ROTATIONYRAD")));
                    jObj.put("rotationZRad", cursor.getDouble(cursor.getColumnIndex("ROTATIONZRAD")));

                    jObj.put("latitude", cursor.getDouble(cursor.getColumnIndex("LATITUDE")));
                    jObj.put("longitude", cursor.getDouble(cursor.getColumnIndex("LONGITUDE")));

                    if (Double.isInfinite(cursor.getDouble(cursor.getColumnIndex("SPEEDLOCATIONMANAGER"))) || Double.isInfinite(cursor.getDouble(cursor.getColumnIndex("SPEEDCALCULATED")))) {
                        System.out.println("infi");
                    }

                    jObj.put("speedLocationManager", cursor.getDouble(cursor.getColumnIndex("SPEEDLOCATIONMANAGER")));
                    jObj.put("speedCalculated", cursor.getDouble(cursor.getColumnIndex("SPEEDCALCULATED")));


                    jObj.put("jerk", cursor.getDouble(cursor.getColumnIndex("JERK")));

                    jObj.put("magneticFieldX", cursor.getDouble(cursor.getColumnIndex("MAGNETICFIELDX")));
                    jObj.put("magneticFieldY", cursor.getDouble(cursor.getColumnIndex("MAGNETICFIELDY")));
                    jObj.put("magneticFieldZ", cursor.getDouble(cursor.getColumnIndex("MAGNETICFIELDZ")));

                    if (Double.isInfinite(cursor.getDouble(cursor.getColumnIndex("GACC")))) {
                        jObj.put("gacc", "Infinite");
                    } else {
                        jObj.put("gacc", cursor.getDouble(cursor.getColumnIndex("GACC")));
                    }

                    if (Double.isInfinite(cursor.getDouble(cursor.getColumnIndex("STEER")))) {
                        jObj.put("steer", "Infinite");
                    } else {
                        jObj.put("steer", cursor.getDouble(cursor.getColumnIndex("STEER")));
                    }

                    if (Double.isInfinite(cursor.getDouble(cursor.getColumnIndex("GBEA")))) {
                        jObj.put("gbea", "Infinite");
                    } else {
                        jObj.put("gbea", cursor.getDouble(cursor.getColumnIndex("GBEA")));
                    }

                    if (Double.isInfinite(cursor.getDouble(cursor.getColumnIndex("JERK")))) {
                        jObj.put("jerk", "Infinite");
                    } else {
                        jObj.put("jerk", cursor.getDouble(cursor.getColumnIndex("JERK")));
                    }

                    jArr.put(jObj);
                } while (cursor.moveToNext());
                cursor.close();
                return jArr;
            }

        } catch (Exception e) {
            e.getMessage().toString();
            File output = new File(IO.baseDir + File.separator + System.currentTimeMillis() / 1000 + "_error.txt");
            IO.writeFile(output, e.getMessage() + ": " + e.getStackTrace());
        }
        return null;

    }

}
