package au.carrsq.sensorplatform.MVNAlgorithm;

import org.apache.commons.math3.distribution.MixtureMultivariateNormalDistribution;
import org.apache.commons.math3.distribution.MultivariateNormalDistribution;
import org.apache.commons.math3.exception.ConvergenceException;
import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.exception.NotStrictlyPositiveException;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.SingularMatrixException;
import org.apache.commons.math3.random.EmpiricalDistribution;
import org.apache.commons.math3.stat.correlation.Covariance;
import org.apache.commons.math3.util.FastMath;
import org.apache.commons.math3.util.MathArrays;
import org.apache.commons.math3.util.Pair;

import java.util.List;
import java.util.Vector;


public class MVNNewClassifier {

    private int nFeatures = 2;//all feature sets are now two-dimensional

    //EM algorithm convergence parameters
    final int maxIterations = 100;
    final double threshold = 10e-5;

    //Counter for re-estimating the distribution parameters (e.g. every 10K samples)
    int bufferIndex = 0;
    double[][] trainingBuffer;
    double[][] updateBuffer;

    private int initialCount = 1500; //re-estimate the distribution every so many samples
    private int reTrainCount = 1000;
    private int alternative_selected = -1;

    //is the system ready to classify?
    boolean distributionTrained = false;

    private double[] currentSample;

    private double determinant = -1;
    private double severity = 0.0d;


    //The (two-sided) quantile threshold of probabilities
    //All observations with prob. lower than 100*probability_limit percent are considered a potential (!) event
    //final double quantile_limit = 0.15;
    final double quantile_limit = 0.01;
    double probability_limit; //Inverse CDF value of the quantile limit (associated probability threshold)
    EmpiricalDistribution probDist;

    //The current estimate of the data distribution
    public MultivariateNormalDistribution mvn;


    /**
     * Project data into feature space (log-transform, products etc.)
     * Code is located here for quick access ;)
     * Project data into feature space (log-transform, products etc.)
     * Code is located here for quick access ;)
     */
    private double[] featureTransform(double[] data) {

        double[] features = new double[nFeatures];
        //index 0: jerk, 1: steer,2: gacc, 3: gbea, 4:speed
        //Remember to set nFeatures
        switch (alternative_selected) {
            case 1:
                features[0] = data[2];// gacc
                features[1] = data[3];//gbea
                break;

            case 2:
                features[0] = data[2] * data[0]; //gacc * jerk * SQRT(speed)
                features[1] = data[3] * data[1]; //gbea * steer * speed
                break;

            case 3:
                features[0] = data[2] * data[0]; //gacc * jerk * SQRT(speed)
                features[1] = data[3] * data[0]; //gbea * steer * SQRT(speed)
                break;
            case 4:
                features[0] = data[2] * data[0] * Math.pow(data[4] + 100, 2);// gacc * jerk
                features[1] = data[3] * data[1] * Math.pow(data[4] + 35, 2);//gbea * steer
                break;

            default:
                System.out.println(alternative_selected + " Not valid!");
                features[0] = data[2];// gacc
                features[1] = data[3] * data[1];//gbea * steer
        }

        return features;
    }


    /**
     * Constructor of the classifier using raw input data (untransformed)
     */
    public MVNNewClassifier(int alternative, int trainSamples, boolean retraining) {

        alternative_selected = alternative;
        initialCount = trainSamples;
        if (!retraining) reTrainCount = 5 * trainSamples; //IT WILL NEVER BE REACHED

        System.out.print("Alternative: " + alternative_selected + " nFeatures: " + nFeatures);
        distributionTrained = false;
        trainingBuffer = new double[initialCount][nFeatures];
        updateBuffer = new double[reTrainCount][nFeatures];

    }

    /**
     * Creates a projection of the data into our feature space.
     * Initially creates our single-component mixture model of a Multivariate Normal (MVN).
     */
    public boolean initialTraining(double[][] data) {

        double[] component_weights = {1};
        double[][] component_means = new double[1][nFeatures];//{{1,2,3,4}}

        //create feature means;
        for (int i = 0; i < nFeatures; i++) {
            for (int j = 0; j < data.length; j++) {
                component_means[0][i] += data[j][i];
            }
        }

        for (int j = 0; j < nFeatures; j++) {
            component_means[0][j] /= data.length;
        }

        //create a first estimate of the covariance matrix
        final double[][] covMat = new Covariance(data).getCovarianceMatrix().getData();
        double[][][] component_covariance_matrices = new double[1][nFeatures][nFeatures];
        component_covariance_matrices[0] = covMat;

        try {
            //Initialize the mixture
            MixtureMultivariateNormalDistribution mvn_init = new MixtureMultivariateNormalDistribution
                    (component_weights, component_means, component_covariance_matrices);

            // Fit the distribution to the features (training)
            mvn = this.fit(mvn_init, data, maxIterations, threshold);
            distributionTrained = true;

            generateECDF();

            //success?
            return true;

        } catch (Exception e) {
            distributionTrained = false;
            bufferIndex = 0;
            e.printStackTrace();
            return false;
        }
    }

    /**
     * This function updates the distribution parameters given a fresh sampleset of data
     * It samples 5000 samples from the current distribution and adds some (retrainCount) new samples
     */
    public boolean updateParameters(double[][] features) throws Exception {
        try {

            Vector<Pair<Double, MultivariateNormalDistribution>> mvn_components = new Vector<Pair<Double, MultivariateNormalDistribution>>();
            Pair<Double, MultivariateNormalDistribution> component = new Pair<Double, MultivariateNormalDistribution>(1.0d, mvn);
            mvn_components.add(component);

            double[][] training_data = new double[features.length + 5000][features[0].length];

            for (int i = 0; i < features.length; i++) {
                training_data[i] = features[i];
            }

            for (int i = features.length; i < features.length + 5000; i++) {
                training_data[i] = mvn.sample();
            }

            MixtureMultivariateNormalDistribution mvn_init = new MixtureMultivariateNormalDistribution(mvn_components);
            mvn = fit(mvn_init, training_data, maxIterations, threshold);

            generateECDF();

            double[][] component_means = new double[1][nFeatures];
            double[][][] component_covariance_matrices = new double[1][nFeatures][nFeatures];

            component_means[0] = mvn.getMeans();
            component_covariance_matrices[0] = mvn.getCovariances().getData();


            String state = saveState(component_means, component_covariance_matrices);

            System.out.print(state + "\n");

            return true;

        } catch (Exception e) {
            return false;
        }

    }


    /**
     * This function updates the distribution parameters given a fresh sampleset of data
     * It samples 5000 samples from the current distribution and adds some (retrainCount) new samples
     */
    public boolean reTrain(double[][] features) throws Exception {
        try {

            long retStart = System.currentTimeMillis();

            Vector<Pair<Double, MultivariateNormalDistribution>> mvn_components = new Vector<Pair<Double, MultivariateNormalDistribution>>();
            Pair<Double, MultivariateNormalDistribution> component = new Pair<Double, MultivariateNormalDistribution>(1.0d, mvn);
            mvn_components.add(component);

            double[][] training_data = new double[features.length + 5000][features[0].length];

            for (int i = 0; i < features.length; i++) {
                training_data[i] = features[i];
            }

            for (int i = features.length; i < features.length + 5000; i++) {
                training_data[i] = mvn.sample();
            }

            MixtureMultivariateNormalDistribution mvn_init = new MixtureMultivariateNormalDistribution(mvn_components);
            mvn = fit(mvn_init, training_data, maxIterations, threshold);

            System.out.print("Retrain lasted: " + Long.toString(System.currentTimeMillis() - retStart));

            return true;

        } catch (Exception e) {
            return false;
        }

    }


    /**
     * This function updates the distribution parameters given a fresh sampleset of data
     * It samples 5000 samples from the current distribution and adds some (retrainCount) new samples
     * <p>
     * Perform anomaly detection and return the estimated event severity for a single data sample
     */
    public double classifySample(double[] sample) throws Exception {

        //check if retraining is necessary
        if (!distributionTrained) return 0.0d;

        double prob, severity;

        //classify
        prob = mvn.density(sample);

        severity = 0.0;

        if (prob < probability_limit)
            severity = 1 - (getEmpiricalQuantile(sample) * 1 / quantile_limit);

        return severity;
    }

    /**
     * Classifies the current sample
     *
     * @return the currently calculated severity
     */
    public double evaluate() {
        try {
            severity = classifySample(currentSample);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return severity;
    }

    /**
     * Calculates the density of the current sample in the MultivariateNormalDistribution
     *
     * @return the currently calculated probability or 1 if the distribution is not trained yet, 0 in case of error.
     */
    public double probability() {

        double probability = 0;
        try {

            if (!distributionTrained) return 1.0d;

            probability = mvn.density(currentSample);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return probability;
    }

    /**
     * Estimates the empirical quantile
     *
     * @param sample
     * @return cumulativeProbability
     */
    private double getEmpiricalQuantile(double[] sample) {
        return probDist.cumulativeProbability(mvn.density(sample));
    }


    /**
     * Set the input of the classifier
     * <p>
     * double[] rawSample = new double[5];
     * rawSample[0] = bundle.getDouble("jerk");
     * rawSample[1] = bundle.getDouble("steer");
     * rawSample[2] = bundle.getDouble("gacc");
     * rawSample[3] = bundle.getDouble("newGbea");
     * rawSample[4] = bundle.getDouble("speed");
     *
     * @param rawSample
     */
    public void setInput(double[] rawSample) {
        if (!Double.isNaN(rawSample[0]) && !Double.isNaN(rawSample[1]) && !Double.isNaN(rawSample[2])
                && !Double.isNaN(rawSample[3]) && !Double.isNaN(rawSample[4])) {


            //convert the raw input into the private currentSample var
            currentSample = new double[nFeatures];
            currentSample = featureTransform(rawSample);


            if (distributionTrained) {
                //Train the system after each retrainCount observed samples
                bufferIndex++;
                updateBuffer[bufferIndex - 1] = currentSample;

                if (bufferIndex >= reTrainCount) {

                    //log.appendLog(fileNameMVN, "Automatically retraining after "+ reTrainCount + " observations");
                    try {
                        analyseCovarianceMatrix(updateBuffer);
                        updateParameters(updateBuffer);
                        //print();
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    bufferIndex = 0;
                }
            } else {
                //Train the system for the first time
                bufferIndex++;
                trainingBuffer[bufferIndex - 1] = currentSample;
                if (bufferIndex >= initialCount) {
                    initialTraining(trainingBuffer);
                    bufferIndex = 0;
                    //if(mvn!=null) print();
                }
            }
        }
    }


    /**
     * Perform anomaly detection and return the estimated event severity for every data sample
     *
     * @param samples
     * @return
     */
    public double[] classifySamples(double[][] samples) {

        double[] prob = new double[samples.length];
        double[] severity = new double[samples.length];


        if (!distributionTrained) return severity;

        for (int i = 0; i < samples.length; i++) {
            prob[i] = mvn.density(samples[i]);
            severity[i] = 0;
            if (prob[i] < probability_limit)
                severity[i] = 1 - (getEmpiricalQuantile(samples[i]) * 1 / quantile_limit);
        }

        return severity;
    }


    /**
     * Print out the parameters and covariances
     */
    public void print() {

        System.out.println("-----------------------");
        System.out.println("MVN Parameters:");
        System.out.println("Means:");
        for (int i = 0; i < nFeatures; i++) {
            System.out.println(mvn.getMeans()[i]);
        }


        System.out.println("Covariances:");
        System.out.println(mvn.getCovariances());
        System.out.println("=======================");
    }


    /**
     * @param initialMixture
     * @param data
     * @param maxIterations
     * @param threshold
     * @return
     * @throws SingularMatrixException
     * @throws NotStrictlyPositiveException
     * @throws DimensionMismatchException
     */
    private MultivariateNormalDistribution fit(final MixtureMultivariateNormalDistribution initialMixture,
                                               double[][] data,
                                               final int maxIterations,
                                               final double threshold)
            throws SingularMatrixException,
            NotStrictlyPositiveException,
            DimensionMismatchException {
        if (maxIterations < 1) {
            throw new NotStrictlyPositiveException(maxIterations);
        }
        if (threshold < Double.MIN_VALUE) {
            throw new NotStrictlyPositiveException(threshold);
        }

        final int n = data.length;
        // Number of data columns. Jagged data already rejected in constructor,
        // so we can assume the lengths of each row are equal.
        final int numCols = data[0].length;
        final int k = initialMixture.getComponents().size();

        final int numMeanColumns = initialMixture.getComponents().get(0).getSecond().getMeans().length;
        if (numMeanColumns != numCols) {
            throw new DimensionMismatchException(numMeanColumns, numCols);
        }

        int numIterations = 0;
        double previousLogLikelihood = 0d;
        double logLikelihood = Double.NEGATIVE_INFINITY;
        // Initialize model to fit to initial mixture.
        MixtureMultivariateNormalDistribution fittedModel = new MixtureMultivariateNormalDistribution(initialMixture.getComponents());

        while (numIterations++ <= maxIterations &&
                FastMath.abs(previousLogLikelihood - logLikelihood) > threshold) {
            previousLogLikelihood = logLikelihood;
            double sumLogLikelihood = 0d;

            // Mixture components
            final List<Pair<Double, MultivariateNormalDistribution>> components
                    = fittedModel.getComponents();

            // Weight and distribution of each component
            final double[] weights = new double[k];

            final MultivariateNormalDistribution[] mvns = new MultivariateNormalDistribution[k];

            for (int j = 0; j < k; j++) {
                weights[j] = components.get(j).getFirst();
                mvns[j] = components.get(j).getSecond();
            }

            final double[][] gamma = new double[n][k];

            // Sum of gamma for each component
            final double[] gammaSums = new double[k];

            // Sum of gamma times its row for each each component
            final double[][] gammaDataProdSums = new double[k][numCols];

            for (int i = 0; i < n; i++) {
                final double rowDensity = fittedModel.density(data[i]);
                sumLogLikelihood += FastMath.log(rowDensity);

                for (int j = 0; j < k; j++) {
                    gamma[i][j] = weights[j] * mvns[j].density(data[i]) / rowDensity;
                    gammaSums[j] += gamma[i][j];

                    for (int col = 0; col < numCols; col++) {
                        gammaDataProdSums[j][col] += gamma[i][j] * data[i][col];
                    }
                }
            }

            logLikelihood = sumLogLikelihood / n;

            // M-step: compute the new parameters based on the expectation
            // function.
            final double[] newWeights = new double[k];
            final double[][] newMeans = new double[k][numCols];

            for (int j = 0; j < k; j++) {
                newWeights[j] = gammaSums[j] / n;
                for (int col = 0; col < numCols; col++) {
                    newMeans[j][col] = gammaDataProdSums[j][col] / gammaSums[j];
                }
            }

            // Compute new covariance matrices
            final RealMatrix[] newCovMats = new RealMatrix[k];
            for (int j = 0; j < k; j++) {
                newCovMats[j] = new Array2DRowRealMatrix(numCols, numCols);
            }
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < k; j++) {
                    final RealMatrix vec
                            = new Array2DRowRealMatrix(MathArrays.ebeSubtract(data[i], newMeans[j]));
                    final RealMatrix dataCov
                            = vec.multiply(vec.transpose()).scalarMultiply(gamma[i][j]);
                    newCovMats[j] = newCovMats[j].add(dataCov);
                }
            }

            // Converting to arrays for use by fitted model
            final double[][][] newCovMatArrays = new double[k][numCols][numCols];
            for (int j = 0; j < k; j++) {
                newCovMats[j] = newCovMats[j].scalarMultiply(1d / gammaSums[j]);
                newCovMatArrays[j] = newCovMats[j].getData();
            }


            // Update current model
            fittedModel = new MixtureMultivariateNormalDistribution(newWeights,
                    newMeans,
                    newCovMatArrays);
        }

        if (FastMath.abs(previousLogLikelihood - logLikelihood) > threshold) {
            // Did not converge before the maximum number of iterations
            throw new ConvergenceException();
        }


        return fittedModel.getComponents().get(0).getSecond();
    }

    /**
     * Calculates the determinant
     *
     * @param newData
     */
    private void analyseCovarianceMatrix(double[][] newData) {
        //create a first estimate of the covariance matrix
        final double[][] covMat = new Covariance(newData).getCovarianceMatrix().getData();
        double[][][] component_covariance_matrices = new double[1][nFeatures][nFeatures];
        component_covariance_matrices[0] = covMat;

        double[][] component_means = new double[1][nFeatures];//{{1,2,3,4}}

        for (int i = 0; i < nFeatures; i++) {
            for (int j = 0; j < newData[0].length; j++) {
                component_means[0][i] += newData[j][i];
            }
        }

        for (int j = 0; j < newData[0].length; j++) {
            component_means[0][j] /= newData.length;
        }

        if (nFeatures == 2) {
            determinant = (covMat[0][0] * covMat[1][1]) - (covMat[0][1] * covMat[1][0]);
        }

        if (nFeatures == 3) {
            determinant = (covMat[0][0] * (covMat[1][1] * covMat[2][2] - covMat[2][1] * covMat[1][2])
                    - covMat[1][0] * (covMat[0][1] * covMat[2][2] - covMat[2][1] * covMat[0][2])
                    + covMat[2][0] * (covMat[0][1] * covMat[1][2] - covMat[1][1] * covMat[0][2]));
        }

    }

    /**
     * Evaluates the mvn randomly to generate the ECDF and quantiles for the threshold
     * and calculates the probability_limit
     */
    private void generateECDF() {
        probDist = new EmpiricalDistribution();

        int samples = 250000;

        double[] probDistData = new double[samples];

        for (int i = 0; i < samples; i++) {
            //random samples to form our ecdf estimate of the probability distribution
            probDistData[i] = mvn.density(mvn.sample());
        }
        probDist.load(probDistData);

        probability_limit = probDist.inverseCumulativeProbability(quantile_limit);
    }

    /**
     * Generates a String representing the components
     * @param component_means
     * @param component_covariance_matrices
     * @return
     */
    private String saveState(double[][] component_means, double[][][] component_covariance_matrices) {

        int rows_mean = component_means[0].length;
        String string_means = Integer.toString(rows_mean).concat("	");

        for (int i = 0; i < rows_mean; i++) {
            string_means = string_means.concat(Double.toString(component_means[0][i]));
            string_means = string_means.concat("	");
        }

        for (int i = 0; i < rows_mean; i++) {
            for (int j = 0; j < rows_mean; j++) {
                string_means = string_means.concat(Double.toString(component_covariance_matrices[0][i][j]));
                string_means = string_means.concat("	");
            }
        }
        string_means = string_means.concat(Double.toString(determinant));
        string_means = string_means.concat("	");

        return (string_means);
    }

    /**
     *
     * @return alternative_selected
     */
    public int getAlternativeSelected() {
        return alternative_selected;
    }

}