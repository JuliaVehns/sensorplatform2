package au.carrsq.sensorplatform.MVNAlgorithm;

/**
 * Created by julia on 08.08.2017.
 */

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


public class MVNTest {

    static String training_filename;
    static String path_RScript;
    static String path_RScript2;
    static String evaluation_filename;
    static int num_alternatives;
    static int log_index;
    static int current_log_index;
    static BufferedWriter outLikelihood;

    public static void main(String[] args) {

        if (args.length == 0) {
            System.out.println("No arguments passed in. Go to 'Run Configurations' -> Arguments. " +
                    "\nFormat: log_index num_alternatives training_filename path_RScript path_RScript2" +
                    "\nExample: 0 4 log-BMW /rscript1 /rscrip2");
            return;
        }

        if (args.length != 5) {
            System.out.println("Wrong quantity of arguments. Should be 5. Exit.");
            return;
        }

        training_filename = args[2]; // without the .txt extension
        path_RScript = args[3];
        path_RScript2 = args[4];
        evaluation_filename = "";
        log_index = -1;
        num_alternatives = -1;

        try {
            log_index = Integer.parseInt(args[0]);
            current_log_index = log_index;
            num_alternatives = Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
            System.out.println("1st and 2nd Arguments must be Numeric. Exit.");
            return;
        }

        try {
            outLikelihood = new BufferedWriter(new FileWriter("likelihood-" + current_log_index + ".txt"));
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        if (log_index == 0)
            for (int i = 1; i <= 4; i++) {
                process(i);
                current_log_index = i;
            }
        else
            process(log_index);


        if (log_index == 0) evaluation_filename = "";

        try {
            String working_directory = System.getProperty("user.dir");
            System.out.println(working_directory);
            System.out.println("Invoking Rscript2 " + path_RScript2);
            System.out.println("Rscript " + path_RScript2 + " "
                    + working_directory + " " + evaluation_filename);
            Process p = Runtime.getRuntime().exec("Rscript " + path_RScript2 + " "
                    + working_directory + " " + evaluation_filename);

            BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line = null;

            while ((line = in.readLine()) != null) {
                System.out.println(line);
            }

            System.out.println("End of invocation of Rscript2.");

        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            outLikelihood.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void process(int log_index) {
        switch (log_index) {
            case 1:
                System.out.println("- Acceleration-Braking Selected -");
                evaluation_filename = "log-ACC-BRK";
                break;
            case 2:
                System.out.println("- Lane Change 3 Selected -");
                evaluation_filename = "log-LC3";
                break;
            case 3:
                System.out.println("- Slalom Selected -");
                evaluation_filename = "log-SLALOM";
                break;
            case 4:
                System.out.println("- U-Turn Selected -");
                evaluation_filename = "log-UTURN";
                break;
            case 5:
                System.out.println("- TRAINLUX Selected -");
                evaluation_filename = "log-TRAINLUX";
                break;
// @Julia never comes to case 6 - 8
            case 6:
                System.out.println("- TRAINCALM Selected -");
                evaluation_filename = "log-traincalm";
                break;

            case 7:
                System.out.println("- TRAINAGG Selected -");
                evaluation_filename = "log-trainagg";
                break;

            case 8:
                System.out.println("- BMW Selected -");
                evaluation_filename = "log-BMW";
                break;

            default:
                System.out.println("First argument should be in the range of 1-4. Exit.");
                return;
        }

        // @Julia feedData(log-BMW, log-ACC-BRK, 4)
        script1(training_filename, evaluation_filename, num_alternatives);
        System.out.println("");

        try {
            String working_directory = System.getProperty("user.dir");
            System.out.println("Invoking Rscript1 " + path_RScript);
            Process p = Runtime.getRuntime().exec("Rscript " + path_RScript + " "
                    + working_directory + " " + evaluation_filename + " " + num_alternatives);

            BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line = null;

            while ((line = in.readLine()) != null) {
                System.out.println(line);
            }

            System.out.println("End of invocation of Rscript1.");
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("");
        System.out.println("- Program exits. -");

    }

    // @Julia feedData(log-BMW, log-ACC-BRK, 4)
    public static void script1(String training_filename, String evaluation_filename, int num_alternatives) {

        System.out.println("");
        int countLines = countLines(training_filename + ".txt");
        System.out.println("Training file " + training_filename + " contains: " + countLines + " lines.");
        if (countLines == 0) {
            System.out.println("File doesn't contain lines. Exit.");
            return;
        }

        // Iterates Feature Sets
        // @Julia produces 4 MVNClassifiers
        for (int i = 1; i < num_alternatives + 1; i++) {

            System.out.println("");
            // @Julia new MVNClassifier(int alternative, int trainSamples, boolean retraining)
            MVNNewClassifier classifier = new MVNNewClassifier(i, countLines, false);

            long training_time = System.currentTimeMillis();
            System.out.println("Training MVN with Feature Set # " + i);
            System.out.println("This may take several seconds...");

            processSamples(training_filename, classifier, false);

            System.out.println("Training finished in " + Long.toString(System.currentTimeMillis() - training_time) + " miliseconds.");
            System.out.println("");
            System.out.println("Evaluating MVN with Feature Set # " + i);
            System.out.println("This may take several seconds...");
            long evaluating_time = System.currentTimeMillis();

            processSamples(evaluation_filename, classifier, true);

            System.out.println("Evaluation finished in " + Long.toString(System.currentTimeMillis() - evaluating_time) + " miliseconds.");
        }

    }

    // @Julia processSamples(training_filename, classifier, false)
    public static void processSamples(String filename, MVNNewClassifier classifier, boolean evaluate) {
        BufferedReader in;
        BufferedWriter out;
        try {
            in = new BufferedReader(new FileReader(filename + ".txt"));
            // @Julia delete file from calculation before
            deleteFile(filename + "-" + classifier.getAlternativeSelected() + ".txt");
            out = new BufferedWriter(new FileWriter(filename + "-" + classifier.getAlternativeSelected() + ".txt"));
            String line = null;
            double[] data = new double[5];

            double likelihood = 0;

            // Iterates samples
            while ((line = in.readLine()) != null) {
                if (line.length() == 0) break;

                // @Julia splits input by tabulator
                String[] dataLine = line.split("\t");

                if (!dataLine[0].equals("rel_time")) {
                    //System.out.println(line);
                    data[0] = Double.parseDouble(dataLine[7]); //jerk
                    data[1] = Double.parseDouble(dataLine[9]); //steer
                    data[2] = Double.parseDouble(dataLine[10]); //gacc
                    data[3] = Double.parseDouble(dataLine[12]); //newGbea
                    data[4] = Double.parseDouble(dataLine[4]); //speed

                    classifier.setInput(data);

                    if (evaluate) {
                        double severity = classifier.evaluate();
                        out.write(dataLine[0] + "\t" + Double.toString(severity) + "\n");


                        double probability = classifier.probability();
                        if (probability == 0)
                            System.out.print(line);
                        likelihood += Math.log(probability);
                    }
                }
            }

            if (evaluate) {
                System.out.print("Log-likelihood: " + likelihood);
                outLikelihood.write(current_log_index + "	" + classifier.getAlternativeSelected() + "	" + likelihood + "\n");
            }

            in.close();
            out.close();

        } catch (IOException e) {

            e.printStackTrace();
        }
    }


    public static int countLines(String filename) {
        InputStream is;
        try {
            is = new BufferedInputStream(new FileInputStream(filename));


            byte[] c = new byte[1024];
            int count = 0;
            int readChars = 0;
            boolean empty = true;
            while ((readChars = is.read(c)) != -1) {
                empty = false;
                for (int i = 0; i < readChars; ++i) {
                    if (c[i] == '\n') {
                        ++count;
                    }
                }
            }
            is.close();
            return (count == 0 && !empty) ? 1 : count;
        } catch (IOException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static void deleteFile(String fileName) {

        try {
            File file = new File(fileName);
            file.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
