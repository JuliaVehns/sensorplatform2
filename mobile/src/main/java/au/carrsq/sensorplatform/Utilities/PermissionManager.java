package au.carrsq.sensorplatform.Utilities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

/**
 * Bundles all permission request in one static function that can be called on startup.
 * Alternatively, single permissions can be requested during runtime.
 */
public class PermissionManager {


    public static void verifyPermissions(Context c) {
        verifyStoragePermissions(c);
        verifyLocationPermissions(c);
        verifyAccountsPermission(c);
    }

    private static final int REQUEST_LOCATION = 1;
    private static String[] PERMISSIONS_LOCATION = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };

    private static void verifyLocationPermissions(Context context) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions((Activity) context,
                    PERMISSIONS_LOCATION, REQUEST_LOCATION);
        }
    }

    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 2;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private static void verifyStoragePermissions(Context context) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    (Activity) context,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    // Account Permission
    private static final int REQUEST_ACCOUNTS = 3;
    private static String[] PERMISSION_ACCOUNT = {
            Manifest.permission.GET_ACCOUNTS
    };

    public static void verifyAccountsPermission(Context context) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(context, Manifest.permission.BLUETOOTH);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions((Activity) context,
                    PERMISSION_ACCOUNT, REQUEST_ACCOUNTS);
        }
    }


    // Storage Permissions
  /*  private static final int REQUEST_ALL = 6;
    private static String[] PERMISSIONS_ALL = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.CHANGE_WIFI_STATE,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.ACCESS_WIFI_STATE,
            Manifest.permission.PACKAGE_USAGE_STATS
    };*/

    /*private static void verifyAllPermissions(Context context) {
        ActivityCompat.requestPermissions(
                (Activity) context,
                PERMISSIONS_ALL,
                REQUEST_ALL
        );
    }*/
}

