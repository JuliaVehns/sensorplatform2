package au.carrsq.sensorplatform.Utilities;


import android.app.ActivityManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import java.util.List;

import au.carrsq.sensorplatform.Database.SqliteDataProvider;
import au.carrsq.sensorplatform.Models.PhoneInteractionDataVector;
import au.carrsq.sensorplatform.R;
import au.carrsq.sensorplatformshared.UStats;


public class PhoneInteractionService extends Service implements View.OnTouchListener {

    NotificationReceiver nReceiver;
    ScreenUnlockReceiver sReceiver;

    public static final String TAG = "USER_INTERACTION_SRV";
    private LinearLayout touchLayout;

    private SharedPreferences studyPrefs;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        nReceiver = new NotificationReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction("au.carrsq.sensorplatform.NOTIFICATION");
        registerReceiver(nReceiver, filter);

        sReceiver = new ScreenUnlockReceiver();
        IntentFilter filter_screen = new IntentFilter();
        filter_screen.addAction(Intent.ACTION_SCREEN_ON);
        registerReceiver(sReceiver, filter_screen);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Log.d("VERDION_SDK", String.valueOf(Build.VERSION.SDK_INT) );
            if (Settings.canDrawOverlays(getApplicationContext()) == true) {
                addSpyPixel();
            }
        } else {
            addSpyPixel();
        }

        studyPrefs = getApplicationContext().getSharedPreferences(getApplicationContext().getString(R.string.study_preferences_key), Context.MODE_PRIVATE);

        Log.d(TAG, "Service created");
    }

    private final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            byte[] readBuf = (byte[]) msg.obj;
            // construct a string from the valid bytes in the buffer
            String readMessage = new String(readBuf, 0, msg.arg1);
            Log.d(TAG, readMessage);
            if (readMessage.equals("au.carrsq.sensorplatform.STOP")) {
                Log.d(TAG, "Stop received, shut down");
                sendBroadcast(new Intent(readMessage));
                stopSelf();
            }
        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "Service Started");
        return START_STICKY;

    }

    @Override
    public void onDestroy() {
        unregisterReceiver(nReceiver);
        unregisterReceiver(sReceiver);

        // removes SpyLayout
        touchLayout.setOnTouchListener(null);
        super.onDestroy();
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        String foregroundTaskAppName = null;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            foregroundTaskAppName = UStats.getForegroundApp(getApplicationContext());
        } else {
            final ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
            final List<ActivityManager.RunningTaskInfo> recentTasks = activityManager.getRunningTasks(Integer.MAX_VALUE);
            String app = recentTasks.get(0).topActivity.getPackageName();
            PackageManager pm = PhoneInteractionService.this.getPackageManager();
            try {
                PackageInfo foregroundAppPackageInfo = pm.getPackageInfo(app, 0);
                foregroundTaskAppName = foregroundAppPackageInfo.applicationInfo.loadLabel(pm).toString();
            } catch (Exception e) {
                Log.d("Exception: ", e.getMessage().toString());
            }
        }

        Log.d(TAG, "Touched! " + foregroundTaskAppName);
//        Toast.makeText(getApplicationContext(), "User-interaction: " + foregroundTaskAppName, Toast.LENGTH_SHORT).show();

        PhoneInteractionDataVector interaction = new PhoneInteractionDataVector(studyPrefs.getString("code_id", ""), foregroundTaskAppName);
        addPhoneInteractionDataToProvider(interaction);


        return false;
    }

    private void addSpyPixel() {
        touchLayout = new LinearLayout(this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(1, 1);

        touchLayout.setLayoutParams(lp);
        touchLayout.setOnTouchListener(this);
        touchLayout.setBackgroundColor(1);
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        WindowManager.LayoutParams mParams = new WindowManager.LayoutParams(
                1, 1,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                        WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |
                        WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                PixelFormat.TRANSLUCENT);
        mParams.gravity = Gravity.START;
        wm.addView(touchLayout, mParams);
    }

    class NotificationReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String notification_event = intent.getStringExtra("notification_event");
            Log.d(TAG, notification_event);
//            Toast.makeText(getApplicationContext(), "User-interaction: Notification " + notification_event, Toast.LENGTH_SHORT).show();

            PhoneInteractionDataVector interaction = new PhoneInteractionDataVector(studyPrefs.getString("code_id", ""), notification_event);
            addPhoneInteractionDataToProvider(interaction);
        }
    }

    class ScreenUnlockReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
                Log.d(TAG, Intent.ACTION_SCREEN_ON);

//                Toast.makeText(getApplicationContext(), "User-interaction: SCREEN_ON", Toast.LENGTH_SHORT).show();
                PhoneInteractionDataVector interaction = new PhoneInteractionDataVector(studyPrefs.getString("code_id", ""), "SCREEN_ON");
                addPhoneInteractionDataToProvider(interaction);

            }
        }
    }

    private void addPhoneInteractionDataToProvider(PhoneInteractionDataVector v) {

        ContentValues contentValues = new ContentValues();
        contentValues.put("code_id", v.getCode_id());
        contentValues.put("timestamp", String.valueOf(v.getTimestamp()));
        contentValues.put("datetime", v.getDateTime());
        contentValues.put("interaction", v.getInteraction());
        contentValues.put("sync", 0);

        if (getApplicationContext() != null) {
            Uri uri = getApplicationContext().getContentResolver().insert(
                    SqliteDataProvider.PHONE_INTERACTION_DATA_URI, contentValues);
        }

    }

}
